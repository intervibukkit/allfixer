package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;

public class stacks implements Listener {
	private AllFixer main;
	stacks(AllFixer a) {
		main = a;
		main.log.info("принудительное стакование активировано");
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onClickInv(InventoryClickEvent event) {
		if (main.conf.stacks) {
			if (!event.isCancelled()) {
				String action = event.getAction().toString();
				ItemStack item = event.getCurrentItem();
				ItemStack cursor = event.getCursor();
				if (action.equalsIgnoreCase("PLACE_ALL") | action.equalsIgnoreCase("PLACE_SOME") && item != null & cursor != null) {
					if (item.isSimilar(cursor)) {
						int amount = cursor.getAmount() + item.getAmount();
						if (amount - item.getMaxStackSize() > 0) {
							item.setAmount(item.getMaxStackSize());
							cursor.setAmount(Math.abs((item.getMaxStackSize() - amount)));
						} else {
							item.setAmount((item.getAmount() + cursor.getAmount()));
							cursor.setAmount(0);
							cursor.setType(Material.valueOf("AIR"));
						}
						event.setCurrentItem(item);
						event.setCursor(cursor);
						event.setCancelled(true);
					}
				} else if (action.equalsIgnoreCase("PLACE_ONE") && item != null & cursor != null) {
					if (item.isSimilar(cursor)) {
						if ((item.getAmount() + 1) <= item.getMaxStackSize()) {
							item.setAmount((item.getAmount() +1));
							if ((cursor.getAmount() - 1) > 0) {
								cursor.setAmount((cursor.getAmount() - 1));
							} else {
								cursor.setAmount(0);
								cursor.setType(Material.valueOf("AIR"));
							}
						}
						event.setCurrentItem(item);
						event.setCursor(cursor);
						event.setCancelled(true);
					}
				}
			}
		}
	}
}