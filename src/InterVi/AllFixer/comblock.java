package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.block.CommandBlock;
import org.bukkit.block.Block;

public class comblock implements Listener {
	AllFixer main;
	private utils ut = new utils("mem");
	comblock(AllFixer all) {main = all; ut.mem.setSize(100); main.log.info("модуль блокировки команд ком блока активирован");};
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void blockCheck(BlockRedstoneEvent event) {
		if(main.conf.comblock == true && event.getBlock().getType().getId() == 137 & event.getBlock().getState() instanceof CommandBlock) {
			 if (event.getNewCurrent() != 0 && event.getOldCurrent() == 0) {
				 Block block = event.getBlock();
				 String command = ((CommandBlock) block.getState()).getCommand().toLowerCase().replaceAll("/", "").trim(); //получаем команду
				 boolean badcom = false;
				 String list[] = new String[main.conf.cblock.length];
				 list = ut.mlcase(main.conf.cblock);
				 for(int i = 0; i < list.length; i++) { //проверяем, не запрещена ли она
					 String ch = ut.remChars(command, list[i].length(), command.length());
					 if(ch.equals(list[i])) {badcom = true; break;} else badcom = false;
				 }
				 if(main.conf.sudofix == true) { //защита от обхода через sudo
					 String com[] = command.split(" ");
					 String cmd = "";
					 for(int i = 2; i < com.length; i++) {
						 cmd += com[i].trim() + " ";
					 }
					 cmd = cmd.trim();
					 if(cmd != null) {
						 for(int i = 0; i < list.length; i++) {
							 if (main.conf.cbvomfix) { //фикс points от Vomine
									if (cmd.indexOf("point add com") > -1) {
										String com1 = cmd;
										com1 = com1.replaceFirst("point add com", "").replaceFirst("vom", "").replaceFirst("ine", "").trim();
										String coms[] = com1.split(",");
										for (int n = 0; n < coms.length; n++) {
											if (ut.remChars(coms[n], list[n].length(), coms[n].length()).trim().equalsIgnoreCase(list[i])) {
												badcom = true;
												break;
											}
										}
									}
								}
							 if (!badcom) {
							 	String ch = ut.remChars(cmd, list[i].length(), cmd.length());
							 	if(ch.equals(list[i])) {badcom = true; break;}
							 }
						 }
					 }
				 }
				 
				 String mir = block.getLocation().getWorld().getName();
				 int x = block.getLocation().getBlockX();
				 int y = block.getLocation().getBlockY();
				 int z = block.getLocation().getBlockZ();
				 String loc = mir + " " + x + " " + y + " " + z;
				 String check = loc + command;
				 
				 if(badcom == true) {
					 if(!event.getBlock().setTypeId(main.conf.cb_replace)) main.log.info("неудачная попытка заменить ком блок на " + loc);
					 event.setNewCurrent(event.getOldCurrent());
					 if(ut.mem.isVal(check.hashCode()) == false) {
						ut.mem.sendValue(check.hashCode());
					 	if (main.conf.pctocons == true) main.log.info("на " + loc + " ком блок пытался выполнить: " + command);
					 	if (main.conf.pctogame == true) main.sendMes("на " + loc + " ком блок пытался выполнить: " + command);
					 	if (main.conf.gamelog == true) main.sendGlog(null, "на " + loc + " ком блок пытался выполнить: " + command);
					 }
				 }
			 }
		}
	}
	
}