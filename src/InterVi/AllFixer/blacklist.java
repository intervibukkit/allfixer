package InterVi.AllFixer;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.block.BlockExpEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Iterator;
import org.bukkit.enchantments.Enchantment;


public class blacklist implements Listener {
	AllFixer main;
	private utils ut = new utils("mem");
	blacklist(AllFixer all) {main = all; ut.mem.setSize(7000); main.log.info("черный список активирован");};
	
	//запрет на установку блоков
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockPlace(BlockPlaceEvent event) {
		if (main.conf.blacklist == true && main.conf.place == true) {
		Player player = event.getPlayer();
		Block block = event.getBlock();
		for (int i = 0; i < main.conf.placeitems.length; i++) {if (event.getBlock().getTypeId() == main.conf.placeitems[i] && player != null && player.isOp() == false) {
			if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.noplace") == true) {break;};
			String mir = block.getLocation().getWorld().getName();
			if (ut.chW(mir, main.conf.plworlds) == true) break;
			event.setCancelled(true);
			int x = block.getLocation().getBlockX();
			int y = block.getLocation().getBlockY();
			int z = block.getLocation().getBlockZ();
			if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("игрок " + player.getName() + " пытался ставить " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
			if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("игрок " + player.getName() + " пытался ставить " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
			if (main.conf.gamelog == true) main.sendGlog(player.getName(), "пытался ставить " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
			break;
		}}}}
	
	//запрет на ломание блоков
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockBreak(BlockBreakEvent event) {
		if (main.conf.blacklist == true && main.conf.bbreak == true) {
			Player player = event.getPlayer();
			Block block = event.getBlock();
			for (int i = 0; i < main.conf.breakitems.length; i++) {if (event.getBlock().getTypeId() == main.conf.breakitems[i] && player != null && player.isOp() == false) {
				if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.nobreak") == true) {break;};
				String mir = block.getLocation().getWorld().getName();
				if (ut.chW(mir, main.conf.breworlds) == true) break;
				event.setCancelled(true);
				int x = block.getLocation().getBlockX();
				int y = block.getLocation().getBlockY();
				int z = block.getLocation().getBlockZ();
				if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("игрок " + player.getName() + " пытался ломать " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("игрок " + player.getName() + " пытался ломать " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.gamelog == true) main.sendGlog(player.getName(), "пытался ломать " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
				break;
	}}}}
	
	//запрет на урон блокам
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockDamage(BlockDamageEvent event) {
		if (main.conf.blacklist == true && main.conf.damage == true) {
			Player player = event.getPlayer();
			Block block = event.getBlock();
			for (int i = 0; i < main.conf.damageitems.length; i++) {if (event.getBlock().getTypeId() == main.conf.damageitems[i] && player != null && player.isOp() == false) {
				if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.nodamage") == true) {break;};
				String mir = block.getLocation().getWorld().getName();
				if (ut.chW(mir, main.conf.damworlds) == true) break;
				event.setCancelled(true);
				int x = block.getLocation().getBlockX();
				int y = block.getLocation().getBlockY();
				int z = block.getLocation().getBlockZ();
				if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("игрок " + player.getName() + " пытался нанести урон " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("игрок " + player.getName() + " пытался нанести урон " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.gamelog == true) main.sendGlog(player.getName(), "пытался нанести урон " + block.getType().toString() + " по координатам " + mir + " - " + x + " " + y + " " + z);
				break;
	}}}}
	
	//запрет на натуральный поджог блоков
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockIngite(BlockIgniteEvent event) {
		if (main.conf.blacklist == true && main.conf.ingite == true && event.getPlayer() == null) {
			Block block = event.getBlock();
			for (int i = 0; i < main.conf.ingiteitems.length; i++) {if (block.getTypeId() == main.conf.ingiteitems[i]) {
				String mir = block.getLocation().getWorld().getName();
				if (ut.chW(mir, main.conf.ligworlds) == true) break;
				event.setCancelled(true); break;
	}}}}
	
	//запрет на поджог блоков игроком
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockIngitePlay(PlayerInteractEvent event) {
		//потому что BlockIgniteEvent всегда думает, что поджигают воздух :(
		if (main.conf.blacklist == true && main.conf.ingite == true && event.getPlayer() != null && event.getPlayer().isOp() == false && event.getAction().toString() == "RIGHT_CLICK_BLOCK") {
			Player player = event.getPlayer();
			Block block = event.getClickedBlock();
			int ligh = event.getItem().getTypeId();
			String lighn = event.getItem().getType().toString();
			boolean lighter = false;
			for (int i2 = 0; i2 < main.conf.lighter.length; i2++) {if (ligh == main.conf.lighter[i2]) {lighter = true; break;} else lighter = false;};
			String mir = block.getLocation().getWorld().getName();
			int x = block.getLocation().getBlockX();
			int y = block.getLocation().getBlockY();
			int z = block.getLocation().getBlockZ();
			for (int i = 0; i < main.conf.ingiteitems.length; i++) {if (block.getTypeId() == main.conf.ingiteitems[i] && lighter == true) {
				if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.nolight") == true) {break;};
				if (ut.chW(mir, main.conf.ligworlds) == true) break;
				event.setCancelled(true);
				if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("игрок " + player.getName() + " пытался поджечь " + block.getType().toString() + " с помощью " + lighn + " по координатам " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("игрок " + player.getName() + " пытался поджечь " + block.getType().toString() + " с помощью " + lighn + " по координатам " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.gamelog == true) main.sendGlog(player.getName(), "пытался поджечь " + block.getType().toString() + " с помощью " + lighn + " по координатам " + mir + " - " + x + " " + y + " " + z);
				break;
	}}}
		//запрет на взаимодействие с блоками
		if (main.conf.blacklist == true && main.conf.nointeract == true && event.getPlayer() != null && event.getPlayer().isOp() == false && event.getAction().toString() == "RIGHT_CLICK_BLOCK") {
			Player player = event.getPlayer();
			Block block = event.getClickedBlock();
			for (int i = 0; i < main.conf.interitem.length; i++) {
				if (main.conf.interitem[i] == block.getTypeId()) {
					if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.nointeract") == true) {break;};
					String mir = block.getLocation().getWorld().getName();
					if (ut.chW(mir, main.conf.intworlds) == true) break;
					event.setCancelled(true);
					int x = block.getLocation().getBlockX();
					int y = block.getLocation().getBlockY();
					int z = block.getLocation().getBlockZ();
					String loc = mir + " " + x + " " + y + " " + z;
					if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("игрок " + player.getName() + " пытался взаимодействовать с " + block.getType().toString() + " на " + loc);
					if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("игрок " + player.getName() + " пытался взаимодействовать с " + block.getType().toString() + " на " + loc);
					if (main.conf.gamelog == true) main.sendGlog(player.getName(), "пытался взаимодействовать с " + block.getType().toString() + " на " + loc);
					break;
		}}}
	}
	
	//запрет на сгорание блоков
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockBurn(BlockBurnEvent event) {
		if (main.conf.blacklist == true && main.conf.burn == true) {
			Block block = event.getBlock();
			for (int i = 0; i < main.conf.burnitems.length; i++) {if (block.getTypeId() == main.conf.burnitems[i]) {
				String mir = block.getLocation().getWorld().getName();
				if (ut.chW(mir, main.conf.burworlds) == true) break;
				event.setCancelled(true); break;
	}}}}
	
	//запрет на толкание поршнем
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockPiston(BlockPistonExtendEvent event){
		if (main.conf.blacklist == true && main.conf.piston == true) {
			Block blocks[] = new Block[event.getBlocks().size()];
			for (int i = 0; i < blocks.length; i++) {if (event.getBlocks().get(i) != null) blocks[i] = event.getBlocks().get(i);};
			Block block = event.getBlock();
			String mir = block.getLocation().getWorld().getName();
			int x = block.getLocation().getBlockX();
			int y = block.getLocation().getBlockY();
			int z = block.getLocation().getBlockZ();
			for (int i = 0; i < main.conf.pistonitems.length; i++) {
				for (int i2 = 0; i2 < blocks.length; i2++) {
					if (event.getBlocks().get(i2) != null) {
						if (blocks[i2].getTypeId() == main.conf.pistonitems[i]) {
							if (ut.chW(mir, main.conf.pisworlds) == true) break;
							event.setCancelled(true);
							if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("блок " + blocks[i2].getType().toString() + " пытались сдвинуть поршнем на " + mir + " - " + x + " " + y + " " + z);
							if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("блок " + blocks[i2].getType().toString() + " пытались сдвинуть поршнем на " + mir + " - " + x + " " + y + " " + z);
							if (main.conf.gamelog == true) main.sendGlog(null, "блок " + blocks[i2].getType().toString() + " пытались сдвинуть поршнем на " + mir + " - " + x + " " + y + " " + z);
							break;
	}}}}}}
	
	//запрет на выпадение опыта с блоков
	@EventHandler(priority = EventPriority.LOWEST)
	public void BlockExp(BlockExpEvent event) {
		if (main.conf.blacklist == true && main.conf.noexp == true) {
			int block = event.getBlock().getTypeId();
			for (int i = 0; i < main.conf.expitems.length; i++) {
				String mir = event.getBlock().getLocation().getWorld().getName();
				if (ut.chW(mir, main.conf.expworlds) == true) break;
				if (main.conf.expitems[i] == block) {event.setExpToDrop(0); break;};
	}}}
	
	//запрет на поднятие вещей
	@EventHandler(priority = EventPriority.LOWEST)
	public void PickupItem(PlayerPickupItemEvent event) {
		if (main.conf.blacklist == true && main.conf.pickupitem == true && event.getPlayer() != null && event.getPlayer().isOp() == false) {
			int item = event.getItem().getItemStack().getTypeId();
			Player player = event.getPlayer();
			String mir = player.getLocation().getWorld().getName();
			int x = player.getLocation().getBlockX();
			int y = player.getLocation().getBlockY();
			int z = player.getLocation().getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String che = player.getName() + mir + item;
			int check = che.hashCode();
			for (int i = 0; i < main.conf.picitem.length; i++) {
				if (main.conf.picitem[i] == item) {
					if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.nopickup") == true) {break;};
					if (ut.chW(mir, main.conf.picworlds) == true) break;
					event.setCancelled(true);
					String itemn = event.getItem().getItemStack().getType().toString();
					if (main.conf.console == true && main.conf.blacktocons == true && ut.mem.isVal(check) == false) main.log.info("игрок " + player.getName() + " пытался поднять " + itemn + " на " + loc);
					if (main.conf.mesingame == true && main.conf.blacktogame == true && ut.mem.isVal(check) == false) main.sendMes("игрок " + player.getName() + " пытался поднять " + itemn + " на " + loc);
					if (main.conf.gamelog == true && ut.mem.isVal(check) == false) main.sendGlog(player.getName(), "пытался поднять " + itemn + " на " + loc);
					ut.mem.sendValue(check); //защита от мощного флуда
					break;
	}}}}
	
	//запрет на выбрасывание вещей
	@EventHandler(priority = EventPriority.LOWEST)
	public void DropItem(PlayerDropItemEvent event) {
		if (main.conf.blacklist == true && main.conf.dropitem == true && event.getPlayer() != null && event.getPlayer().isOp() == false) {
			int item = event.getItemDrop().getItemStack().getTypeId();
			Player player = event.getPlayer();
			String mir = player.getLocation().getWorld().getName();
			int x = player.getLocation().getBlockX();
			int y = player.getLocation().getBlockY();
			int z = player.getLocation().getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String che = player.getName() + mir + item;
			int check = che.hashCode();
			for (int i = 0; i < main.conf.dropitems.length; i++) {
				if (main.conf.dropitems[i] == item) {
					if (main.conf.usepex == true && player.hasPermission("allfixer.blacklist.nodrop") == true) {break;};
					if (ut.chW(mir, main.conf.droworlds) == true) break;
					event.setCancelled(true);
					String itemn = event.getItemDrop().getItemStack().getType().toString();
					if (main.conf.console == true && main.conf.blacktocons == true && ut.mem.isVal(check) == false) main.log.info("игрок " + player.getName() + " пытался выбросить " + itemn + " на " + loc);
					if (main.conf.mesingame == true && main.conf.blacktogame == true && ut.mem.isVal(check) == false) main.sendMes("игрок " + player.getName() + " пытался выбросить " + itemn + " на " + loc);
					if (main.conf.gamelog == true && ut.mem.isVal(check) == false) main.sendGlog(player.getName(), "пытался выбросить " + itemn + " на " + loc);
					ut.mem.sendValue(check); //защита от флуда
					break;
	}}}}
	
	//запрет на перемещение вещей в сундуке и пр.
	@EventHandler(priority = EventPriority.LOWEST)
	public void ClickItem(InventoryClickEvent event) {
		if (main.conf.blacklist == true && main.conf.clickitem == true && event.getWhoClicked() != null && event.getWhoClicked().isOp() == false) {
			if (event.getCurrentItem() != null) {
			int item = event.getCurrentItem().getTypeId();
			String type = event.getSlotType().toString();
			for (int i = 0; i < main.conf.cliitems.length; i++) {
				if (main.conf.cliitems[i] == item) {
					for (int i2 = 0; i2 < main.conf.clislot.length; i2++) {
					if (type.equalsIgnoreCase(main.conf.clislot[i2])) {
					if (main.conf.usepex == true && event.getWhoClicked().hasPermission("allfixer.blacklist.noclick") == true) {break;};
					Player player = (Player) event.getWhoClicked();
					String mir = player.getLocation().getWorld().getName();
					if (ut.chW(mir, main.conf.cliworlds) == true) break;
					event.setCancelled(true);
					int x = player.getLocation().getBlockX();
					int y = player.getLocation().getBlockY();
					int z = player.getLocation().getBlockZ();
					String loc = mir + " " + x + " " + y + " " + z;
					String name = player.getName();
					String che = name + mir + item;
					int check = che.hashCode();
					String itemn = event.getCurrentItem().getType().toString();
					if (main.conf.console == true && main.conf.blacktocons == true && ut.mem.isVal(check) == false) main.log.info("игрок " + name + " пытался переместить в инвентаре " + itemn + " в слоте " + type + " на " + loc);
					if (main.conf.mesingame == true && main.conf.blacktogame == true && ut.mem.isVal(check) == false) main.sendMes("игрок " + name + " пытался переместить в инвентаре " + itemn + " в слоте " + type + " на " + loc);
					if (main.conf.gamelog == true && ut.mem.isVal(check) == false) main.sendGlog(name, "пытался переместить в инвентаре " + itemn + " в слоте " + type + " на " + loc);
					ut.mem.sendValue(check); //защита от флуда
					break;
	}}}}}}
	if (main.conf.blacklist && main.conf.antichar && event.getWhoClicked() != null) { //удаление нестандартно зачаренных предметов
		Player player = (Player) event.getWhoClicked();
		if (!player.isOp() && !player.hasPermission("allfixer.blacklist.antichar") && !event.isCancelled()) {
			ItemStack[] plinv = player.getInventory().getContents();
			ItemStack[] plarmor = player.getInventory().getArmorContents();
			ItemStack cursor = event.getCursor();
			boolean del = false;
			for (int i = 0; i < plinv.length; i++) { //проверка инвентаря
				if (plinv[i] != null) {
					if (!plinv[i].getEnchantments().isEmpty()) {
						Map<Enchantment, Integer> map = plinv[i].getEnchantments();
						Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
						boolean d = false;
						while (iter.hasNext()) {
							Map.Entry<Enchantment, Integer> entry = iter.next();
							int maxlvl = entry.getKey().getMaxLevel();
							int lvl = entry.getValue().intValue();
							if (lvl > maxlvl | lvl < 0) {
								del = true;
								d = true;
								break;
							}
						}
						if (d) {
							player.getInventory().remove(plinv[i]);
							break;
						}
					}
				}
			}
			for (int i = 0; i < plarmor.length; i++) { //проверка брони
				if (plarmor[i] != null) {
					if (!plarmor[i].getEnchantments().isEmpty()) {
						Map<Enchantment, Integer> map = plarmor[i].getEnchantments();
						Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
						boolean d = false;
						while (iter.hasNext()) {
							Map.Entry<Enchantment, Integer> entry = iter.next();
							int maxlvl = entry.getKey().getMaxLevel();
							int lvl = entry.getValue().intValue();
							if (lvl > maxlvl | lvl < 0) {
								del = true;
								d = true;
								break;
							}
						}
						if (d) {
							player.getInventory().remove(plarmor[i]);
							break;
						}
					}
				}
			}
			if (cursor != null) { //проверка курсора
				if (!cursor.getEnchantments().isEmpty()) {
					Map<Enchantment, Integer> map = cursor.getEnchantments();
					Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
					boolean d = false;
					while (iter.hasNext()) {
						Map.Entry<Enchantment, Integer> entry = iter.next();
						int maxlvl = entry.getKey().getMaxLevel();
						int lvl = entry.getValue().intValue();
						if (lvl > maxlvl | lvl < 0) {
							del = true;
							d = true;
							break;
						}
					}
					if (d) {
						event.setCursor(new ItemStack(0));
					}
				}
			}
			if (del) { //отправка сообщения об удалении
				String mir = player.getLocation().getWorld().getName();
				event.setCancelled(true);
				int x = player.getLocation().getBlockX();
				int y = player.getLocation().getBlockY();
				int z = player.getLocation().getBlockZ();
				String loc = mir + " " + x + " " + y + " " + z;
				String name = player.getName();
				if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("у игрока " + name + " был нестандартно зачаренный предмет на " + loc);
				if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("у игрока " + name + " был нестандартно зачаренный предмет на " + loc);
				if (main.conf.gamelog == true) main.sendGlog(name, "был нестандартно зачаренный предмет на " + loc);
			}
		}
	}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityExplosion(EntityExplodeEvent event) { //запрет взрыва заданных блоков сущностью
		if (main.conf.blacklist && main.conf.boomblock) {
			boolean noboom = false;
			String boomber = event.getEntityType().toString();
			for (int i = 0; i < main.conf.boombers.length; i++) {
				if (boomber.equalsIgnoreCase(main.conf.boombers[i])) {noboom = true; break;}
			}
			if (main.conf.noentityboom) event.setCancelled(true); //отменяем взрыв, если так задано в конфиге
			if (main.conf.allbentity) noboom = true;
			if (noboom && !main.conf.noentityboom) { //если взрывается заданная сущность
				boolean stop = false;
				int d = 0;
				do { //основной цикл проверки (все из-за долбанного класса List с его тупыми заморочками)
					d++;
					if (d > 2000) stop = true; //экстренный прерыватель
					int b = event.blockList().size();
					for (int i = 0; i < b; i++) { //проверяем
						Block block = event.blockList().get(i);
						boolean rem = false;
						if (block != null) {
							for (int i2 = 0; i2 < main.conf.boomblocks.length; i2++) {
								if (block.getTypeId() == main.conf.boomblocks[i2]) { //если пытались взорвать запрещенный блок
									rem = true;
									break;
								}
							}
							if (rem) {
								event.blockList().remove(i); //удаляем его из списка
								break; //повторяем цикл заново
							}
						}
						if ((i+1) == b) stop = true; //если пройден весь цикл, можно завершить основной цикл
					}
				} while (!stop);
	}}}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onBlockExplosion(BlockExplodeEvent event) { //запрет взрыва заданных блоков блоком
		if (main.conf.blacklist && main.conf.boomblock) {
			boolean noboom = false;
			int boomber = event.getBlock().getTypeId();
			for (int i = 0; i < main.conf.blockboombers.length; i++) {
				if (boomber == main.conf.blockboombers[i]) {noboom = true; break;}
			}
			if (main.conf.noblockboom) event.setCancelled(true); //отменяем взрыв, если так задано в конфиге
			if (main.conf.allbblock) noboom = true;
			if (noboom && !main.conf.noblockboom) { //если взрывается заданный блок
				boolean stop = false;
				int d = 0;
				do { //основной цикл проверки (все из-за долбанного класса List с его тупыми заморочками)
					d++;
					if (d > 2000) stop = true; //экстренный прерыватель
					int b = event.blockList().size();
					for (int i = 0; i < b; i++) { //проверяем
						Block block = event.blockList().get(i);
						boolean rem = false;
						if (block != null) {
							for (int i2 = 0; i2 < main.conf.boomblocks.length; i2++) {
								if (block.getTypeId() == main.conf.boomblocks[i2]) { //если пытались взорвать запрещенный блок
									rem = true;
									break;
								}
							}
							if (rem) {
								event.blockList().remove(i); //удаляем его из списка
								break; //повторяем цикл заново
							}
						}
						if ((i+1) == b) stop = true; //если пройден весь цикл, можно завершить основной цикл
					}
				} while (!stop);
	}}}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onFireFade(BlockFadeEvent event) { //запрет потухания огня
		if (main.conf.firenoout) {
			Block block = event.getBlock();
			if (block.getTypeId() == 51) { //если это огонь
				Block b = block.getRelative(0, -1, 0);
				if (block.getFace(b) == null) b = null; //проверка, стоит ли огонь на данном блоке
				if (b != null) { //если да
					for (int i = 0; i < main.conf.fnoblocks.length; i++) {
						if (b.getTypeId() == main.conf.fnoblocks[i]) { //если заданный блок - отмена потухания
							event.setCancelled(true);
							break;
						}
					}
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onOpenInv(InventoryOpenEvent event) { //удаление нестандартно зачаренных предметов
		if (main.conf.blacklist && main.conf.antichar && event.getPlayer() != null) {
			Player player = (Player) event.getPlayer();
			if (!player.isOp() && !player.hasPermission("allfixer.blacklist.antichar") && !event.isCancelled()) {
				ItemStack[] plinv = player.getInventory().getContents();
				ItemStack[] plarmor = player.getInventory().getArmorContents();
				ItemStack[] inv = event.getView().getTopInventory().getContents();
				ItemStack cursor = event.getView().getCursor();
				boolean del = false;
				for (int i = 0; i < plinv.length; i++) { //проверка инвентаря
					if (plinv[i] != null) {
						if (!plinv[i].getEnchantments().isEmpty()) {
							Map<Enchantment, Integer> map = plinv[i].getEnchantments();
							Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
							boolean d = false;
							while (iter.hasNext()) {
								Map.Entry<Enchantment, Integer> entry = iter.next();
								int maxlvl = entry.getKey().getMaxLevel();
								int lvl = entry.getValue().intValue();
								if (lvl > maxlvl | lvl < 0) {
									del = true;
									d = true;
									break;
								}
							}
							if (d) {
								player.getInventory().remove(plinv[i]);
								break;
							}
						}
					}
				}
				for (int i = 0; i < plarmor.length; i++) { //проверка брони
					if (plarmor[i] != null) {
						if (!plarmor[i].getEnchantments().isEmpty()) {
							Map<Enchantment, Integer> map = plarmor[i].getEnchantments();
							Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
							boolean d = false;
							while (iter.hasNext()) {
								Map.Entry<Enchantment, Integer> entry = iter.next();
								int maxlvl = entry.getKey().getMaxLevel();
								int lvl = entry.getValue().intValue();
								if (lvl > maxlvl | lvl < 0) {
									del = true;
									d = true;
									break;
								}
							}
							if (d) {
								player.getInventory().remove(plarmor[i]);
								break;
							}
						}
					}
				}
				for (int i = 0; i < inv.length; i++) { //проверка открытого инвентаря
					if (inv[i] != null) {
						if (!inv[i].getEnchantments().isEmpty()) {
							Map<Enchantment, Integer> map = inv[i].getEnchantments();
							Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
							boolean d = false;
							while (iter.hasNext()) {
								Map.Entry<Enchantment, Integer> entry = iter.next();
								int maxlvl = entry.getKey().getMaxLevel();
								int lvl = entry.getValue().intValue();
								if (lvl > maxlvl | lvl < 0) {
									del = true;
									d = true;
									break;
								}
							}
							if (d) {
								event.getView().getTopInventory().remove(inv[i]);
								break;
							}
						}
					}
				}
				if (cursor != null) { //проверка курсора
					if (!cursor.getEnchantments().isEmpty()) {
						Map<Enchantment, Integer> map = cursor.getEnchantments();
						Iterator<Map.Entry<Enchantment, Integer>> iter = map.entrySet().iterator();
						boolean d = false;
						while (iter.hasNext()) {
							Map.Entry<Enchantment, Integer> entry = iter.next();
							int maxlvl = entry.getKey().getMaxLevel();
							int lvl = entry.getValue().intValue();
							if (lvl > maxlvl | lvl < 0) {
								del = true;
								d = true;
								break;
							}
						}
						if (d) {
							event.getView().setCursor(new ItemStack(0));
						}
					}
				}
				if (del) { //отправка сообщения об удалении
					String mir = player.getLocation().getWorld().getName();
					event.setCancelled(true);
					int x = player.getLocation().getBlockX();
					int y = player.getLocation().getBlockY();
					int z = player.getLocation().getBlockZ();
					String loc = mir + " " + x + " " + y + " " + z;
					String name = player.getName();
					if (main.conf.console == true && main.conf.blacktocons == true) main.log.info("у игрока " + name + " был нестандартно зачаренный предмет на " + loc);
					if (main.conf.mesingame == true && main.conf.blacktogame == true) main.sendMes("у игрока " + name + " был нестандартно зачаренный предмет на " + loc);
					if (main.conf.gamelog == true) main.sendGlog(name, "был нестандартно зачаренный предме на " + loc);
				}
			}
		}
	}
}