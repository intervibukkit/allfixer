package InterVi.AllFixer;

//для работы плагина
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;

//для ивентов и вывода в консоль
import java.util.logging.Logger;

//прочее
import org.bukkit.entity.Player;
import InterVi.AllFixer.utils.sort;
import java.util.Date;
import java.util.Iterator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class AllFixer extends JavaPlugin implements Listener {
	config conf = new config();
	private final config conf2 = new config(); //запоминаем опции, заданные изначально
	private String[] load = {"mem", "data", "count", "sort", "list"}; 
	private utils ut = new utils(load);
	private utils ch = new utils(10);
	private utils ch2 = new utils(10);
	public spy spy; //переменная-сслылка на класс spy
	
	class logger { //отправка сообщений в консоль
		private final Logger log = Logger.getLogger("Minecraft");
		public void info(String text) {
			if (conf.shre == true) { //щедящий режим
			if (ch.mem2.colVal(11) == 1) {
				log.info("[AllFixer] " + text);
				ch.mem2.sendValue(11);
			} else
				if (ch.mem2.colVal(11) == 10) {
					ch.mem2.reset();
				} else ch.mem2.sendValue(11);
			} else log.info("[AllFixer] " + text);
		}
	}
	logger log = new logger();
	
	//включение плагина
	public void onEnable() {
		log.info("запуск системных служб");
		if (conf.gamelog == true) {ut.data.setSize(conf.plist, conf.pnlist); gInit();};
		if (conf.helplog == true) {ut.list.setSize(conf.helpsize);};
		log.info("запуск модулей");
		getServer().getPluginManager().registerEvents(this, this);
		getServer().getPluginManager().registerEvents(new comm(this), this);
		if (conf.blacklist == true) getServer().getPluginManager().registerEvents(new blacklist(this), this);
		if (conf.dupefix == true) getServer().getPluginManager().registerEvents(new dupefix(this), this);
		if (conf.antishare == true) getServer().getPluginManager().registerEvents(new antishare(this), this);
		if (conf.cmdfix == true) getServer().getPluginManager().registerEvents(new opencmd(this), this);
		if (conf.jofix == true) getServer().getPluginManager().registerEvents(new joinfix(this), this);
		if (conf.comblock == true) getServer().getPluginManager().registerEvents(new comblock(this), this);
		if (conf.fallfix == true) getServer().getPluginManager().registerEvents(new fallfix(this), this);
		if (conf.playcom == true) getServer().getPluginManager().registerEvents(new comblocker(this), this);
		if (conf.helplog == true) getServer().getPluginManager().registerEvents(new helplog(this), this);
		if (conf.spy == true) getServer().getPluginManager().registerEvents(new spy(this), this);
		if (conf.casefix == true) getServer().getPluginManager().registerEvents(new casefix(this), this);
		if (conf.stacks == true) getServer().getPluginManager().registerEvents(new stacks(this), this);
		log.info("AllFixer запущен");
	}
	//выключение плагина
	public void onDisable() {
		log.info("AllFixer выключен");
	};
	
	//отправка сообщений в игру
	public void sendMes(String mes) {
		Player[] players = new Player[Bukkit.getServer().getOnlinePlayers().size()];
		Iterator<? extends Player> iter = Bukkit.getServer().getOnlinePlayers().iterator();
		for (int i = 0; i < players.length; i++) {
			players[i] = iter.next();
		}
		for (int i = 0; i < players.length; i++){
			boolean ok = true;
			Player player = players[i];
			if (player != null && ut.mem.isVal(player.getName().hashCode()) == false) {
				
				if (conf.shre == true) { //щедящий режим
				if (ch2.mem2.colVal(11) == 1) {
					ok = true;
					ch2.mem2.sendValue(11);
				} else
					if (ch2.mem2.colVal(11) == 10) {
						ch2.mem2.reset();
						ok = false;
					} else {
						ch2.mem2.sendValue(11);
						ok = false;
					}
				}
			
				if (ok == true) {
					boolean issent = false;
					if (player.isOp() == true) {player.sendMessage("[AllFixer] " + mes); issent = true;} //операторам
					if (conf.usepex == true && player.hasPermission("allfixer.mon") == true && issent == false) player.sendMessage("[AllFixer] " + mes); //у кого пермишен
					if (ut.chW(ut.cleform(player.getName()).toLowerCase(), ut.mlcase(conf.allowcomm)) == true && issent == false) player.sendMessage("[AllFixer] " + mes); //кому разрешен доступ
	}}}}
	
	//включение и выключение оповещений для отдельных игроков
	private boolean sets = true;
	public void offPlay(String p) {
		if (this.sets == true) {
			ut.mem.setSize(100000);
			this.sets = false;
		}
		int player = p.hashCode();
		if (ut.mem.isVal(player) == false) ut.mem.sendValue(player);
	}
	public void onPlay(String p) {
		int player = p.hashCode();
		if (ut.mem.isVal(player) == true) ut.mem.delVal(player);
	}
	
	//перезагрузка конфига
	public void reload() {
		conf.load();
		if (conf.blacklist == true && conf2.blacklist == false) getServer().getPluginManager().registerEvents(new blacklist(this), this);
		if (conf.dupefix == true && conf2.dupefix == false) getServer().getPluginManager().registerEvents(new dupefix(this), this);
		if (conf.antishare == true && conf2.antishare == false) getServer().getPluginManager().registerEvents(new antishare(this), this);
		if (conf.cmdfix == true && conf2.cmdfix == false) getServer().getPluginManager().registerEvents(new opencmd(this), this);
		if (conf.jofix == true && conf2.jofix == false) getServer().getPluginManager().registerEvents(new joinfix(this), this);
		if (conf.comblock == true && conf2.comblock == false) getServer().getPluginManager().registerEvents(new comblock(this), this);
		if (conf.fallfix == true && conf2.fallfix == false) getServer().getPluginManager().registerEvents(new fallfix(this), this);
		if (conf.playcom == true && conf2.playcom == false) getServer().getPluginManager().registerEvents(new comblocker(this), this);
		if (conf.helplog == true && conf2.helplog == false) getServer().getPluginManager().registerEvents(new helplog(this), this);
		if (conf.spy == true && conf2.spy == false) getServer().getPluginManager().registerEvents(new spy(this), this);
		if (conf.casefix == true && conf2.casefix == false) getServer().getPluginManager().registerEvents(new casefix(this), this);
		if (conf.stacks == true && conf2.stacks == false) getServer().getPluginManager().registerEvents(new stacks(this), this);
	}
	
	//виртуальный лог
	private String[] noplay;
	public void gInit() {this.noplay = new String[conf.nlist];}
	public void sendGlog(String p, String d) { //отправка в лог
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		if (d != null) d = "[ " + dateFormat.format(date) + " ] " + d;
		if (p != null) { //убираем форматирование
			p = ut.cleform(p);
		}
		
		if (p != null) ut.data.sendData(p.toLowerCase(), d); else {
			int slot = ut.count.get();
			
			if (slot < noplay.length) { //узнаем в какой слот писать значение
				ut.count.next();
			} else {
				ut.count.reset();
				slot = ut.count.get();
				ut.count.next();
			}
			
			if (slot < noplay.length) this.noplay[slot] = d;
		}
	}
	public int getCol() { //получение кол-ва нарушений игроками
		return ut.data.getRealLength();
	}
	public int getNul() { //получение кол-ва безымянных нарушений
		int nu = 0;
		for (int i = 0; i < noplay.length; i++) {
			if (noplay[i] != null) nu++;
		}
		return nu;
	}
	public String getList() { //получение списка нарушителей строкой
		return ut.data.getPlayersString();
	}
	public String[] getNulData() { //получение безымянных нарушений
		String result[] = new String[getNul()];
		int n = 0;
		for (int i = 0; i < noplay.length; i++) {
			if (noplay[i] != null && n < result.length) {result[n] = noplay[i]; n++;};
		}
		return result;
	}
	public String[] getPlayData(String p) { //получение нарушений игрока
		return ut.data.getAllData(p.toLowerCase());
	}
	//--------- общие методы
	public String[] getStr(String[] s, int n) { //получение страницы
		sort r = ut.sort.getSort();
		r.sendData(s);
		r.go();
		return r.getStr(n);
	}
	public int getNumStr(String[] s) { //получение количества страниц
		sort r = ut.sort.getSort();
		r.sendData(s);
		r.go();
		return r.getNumStr();
	}
	//---------
	private void clearNul() { //очистка лога безымянных нарушений
		for (int i = 0; i < noplay.length; i++) {
			noplay[i] = null;
		}
		ut.count.reset();
	}
	
	//логирование helpop
	public void sendHelp(String s) { //отправка в БД
		ut.list.send(s);
	}
	public int getHelpCol() { //получение кол-ва записей
		return ut.list.getCol();
	}
	public String[] getHelpData() { //получение массива сообщений
		return ut.list.getData();
	}
	public void helpClear() { //очистка БД
		ut.list.reset();
	}
	
	PluginManager pm = Bukkit.getPluginManager();
	//включение-выключение модулей
		//черный список
		public void onBlacklist() {
			if (conf2.blacklist == false) {
				conf.blacklist = true;
				pm.registerEvents(new blacklist(this), this);
			} else conf.blacklist = true;
		}
		public void offBlacklist() {
			conf.blacklist = false;
		}
		//фикс дюпов
		public void onDupefix() {
			if (conf2.dupefix == false) {
				conf.dupefix = true;
				pm.registerEvents(new dupefix(this), this);
			} else conf.dupefix = true;
		}
		public void offDupefix() {
			conf.dupefix = false;
		}
		//мониторинг раздач
		public void onAntishare() {
			if (conf2.antishare == false) {
				conf.antishare = true;
				pm.registerEvents(new antishare(this), this);
			} else conf.antishare = true;
		}
		public void offAntishare() {
			conf.antishare = false;
		}
		//открытие ком блока
		public void onCmdfix() {
			if (conf2.cmdfix == false) {
				conf.cmdfix = true;
				pm.registerEvents(new opencmd(this), this);
			} else conf.cmdfix = true;
		}
		public void offCmdfix() {
			conf.cmdfix = false;
		}
		//застревание в блоках при заходе
		public void onJofix() {
			if (conf2.jofix == false) {
				conf.jofix = true;
				pm.registerEvents(new joinfix(this), this);
			} else conf.jofix = true;
		}
		public void offJofix() {
			conf.jofix = false;
		}
		//вывод в консоль
		public void onLogger() {
			conf.console = true;
		}
		public void offLogger() {
			conf.console = false;
		}
		//пермишены
		public void onPex() {
			conf.usepex = true;
		}
		public void offPex() {
			conf.usepex = false;
		}
		//вывод в игру
		public void onGame() {
			conf.mesingame = true;
		}
		public void offGame() {
			conf.mesingame = false;
		}
		//щедящий режим
		public void shOn() {
			conf.shre = true;
		}
		public void shOff() {
			conf.shre = false;
			ch.mem2.reset();
			ch2.mem2.reset();
		}
		//щедящий режим для мониторинга раздач
		public void onAntishre() {
			conf.antishre = true;
		}
		public void offAntishre() {
			conf.antishre = false;
		}
		//фикс падений сквозь блки
		public void onPreload() {
			if (conf2.fallfix == false) {
				conf.fallfix = true;
				pm.registerEvents(new fallfix(this), this);
			} else conf.fallfix = true;
		}
		public void offPreload() {
			conf.fallfix = false;
		}
		//блокиратор команд
		public void onCblock() {
			if (conf2.playcom == false) {
				conf.playcom = true;
				pm.registerEvents(new comblocker(this), this);
			} else conf.playcom = true;
		}
		public void offCblock() {
			conf.playcom = false;
		}
		//виртуальный лог
		public void onGlog() {
			if (conf2.gamelog == false) {
				ut.data.setSize(conf.plist, conf.pnlist);
				gInit();
				conf.gamelog = true;
			} else conf.gamelog = true;
		}
		public void offGlog() {conf.gamelog = false;};
		public void clearGlog() {
			ut.data.clearAll();
			clearNul();
		}
		public boolean clearPlay(String p) {
			boolean result = ut.data.clearPlayer(p.toLowerCase());
			return result;
		}
		//логирование helpop
		public void onHlog() {
			if (conf2.helplog == false) {
				ut.list.setSize(conf.helpsize);
				pm.registerEvents(new helplog(this), this);
				conf.helplog = true;
			} else conf.helplog = true;
		}
		public void offHlog() {
			conf.helplog = false;
		}
		//блокировка команд от ком блока
		public void onComblock() {
			if (conf2.comblock == false) {
				pm.registerEvents(new comblock(this), this);
				conf.comblock = true;
			} else conf.comblock = true;
		}
		public void offComblock() {
			conf.comblock = false;
		}
		//модуль слежки
		public void onSpy() {
			if (conf2.spy == false) {
				pm.registerEvents(new spy(this), this);
				conf.spy = true;
			} else conf.spy = true;
		}
		public void offSpy() {
			conf.spy = false;
		}
		//фикс ников в другом регистре
		public void onCase() {
			if (conf2.casefix == false) {
				pm.registerEvents(new casefix(this), this);
				conf.casefix = true;
			} else {
				conf.casefix = true;
			}
		}
		public void offCase() {
			conf.casefix = false;
		}
		//стакование
		public void onStack() {
			if (conf2.stacks == false) {
				pm.registerEvents(new stacks(this), this);
				conf.stacks = true;
			} else conf.stacks = true;
		}
		public void offStack() {
			conf.stacks = false;
		}
}