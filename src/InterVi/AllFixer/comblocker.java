package InterVi.AllFixer;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.entity.Player;

public class comblocker implements Listener {
	AllFixer main;
	private utils ut = new utils();
	comblocker(AllFixer all) {main = all; main.log.info("блокиратор команд активирован");};
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void commSend(PlayerCommandPreprocessEvent event) {
		if (main.conf.playcom == true && event.getPlayer() != null) {
			boolean pcom = false, pxcom = false;
			Player player = event.getPlayer();
			String com = event.getMessage().toLowerCase().trim();
		
		//блокировка команд для всех, кроме указанных ников
			if (main.conf.allcb == true) {
			for (int i = 0; i < main.conf.comms.length; i++) { //сверка команды со списком
				String com1 = com;
				if (main.conf.vomfix) { //фикс points от Vomine
					if (com1.indexOf("point add com") > -1) {
						com1 = com1.replaceFirst("point add com", "").replaceFirst("vom", "").replaceFirst("ine", "");
						com1 = com1.substring(1, com1.length()).trim();
						String coms[] = com1.split(",");
						for (int n = 0; n < coms.length; n++) {
							if (ut.remChars(("/" + coms[n]), main.conf.comms[n].length(), ("/" + coms[n]).length()).trim().equalsIgnoreCase(main.conf.comms[i])) {
								pcom = true;
								break;
							}
						}
					}
				}
				if (!pcom) {
					String com2 = ut.remChars(com1, 0, com1.indexOf(" ")); com2 = "/" + ut.remChars(com2, main.conf.comms[i].length(), com2.length()).trim();
					String com3 = ut.remChars(com2, 0, com2.indexOf(" ")); com3 = "/" + ut.remChars(com3, main.conf.comms[i].length(), com3.length()).trim();
					com1 = ut.remChars(com1, main.conf.comms[i].length(), com1.length()).trim();
					if (com1.equalsIgnoreCase(main.conf.comms[i]) | com2.equalsIgnoreCase(main.conf.comms[i]) | com3.equalsIgnoreCase(main.conf.comms[i])) {pcom = true; break;} else {pcom = false;};
				}
			}
			if (pcom == true) {
				String p = player.getName();
				p = ut.cleform(p); //чистка форматирования
				if (ut.chW(p, main.conf.comisk) == false && ut.chW(player.getWorld().getName(), main.conf.pcworlds) == false) {
					event.setCancelled(true);
					String name = player.getName();
					if (main.conf.pctocons == true) main.log.info("игрок " + name + " пытался использовать запрещенную команду " + com);
					if (main.conf.pctogame == true) main.sendMes("игрок " + name + " пытался использовать запрещенную команду " + com);
					if (main.conf.gamelog == true) main.sendGlog(name, "пытался использовать запрещенную команду " + com);
					player.sendMessage(main.conf.pcmess);
			}}}
		
		//блокировка команд для всех, у кого нет пермишена и кто не оператор
			if (main.conf.pexblockcom == true) {
			for (int i = 0; i < main.conf.pexcom.length; i++) { //сверка команды со списком
				String com2 = com;
				com2 = ut.remChars(com2, main.conf.pexcom[i].length(), com2.length());
				if (ut.chW(com2, main.conf.pexcom) == true) {pxcom = true; break;} else {pxcom = false;};
			}
			if (pxcom == true) {
				if (player.isOp() == false && player.hasPermission("allfixer.nocomblock") == false && ut.chW(player.getWorld().getName(), main.conf.pexworlds) == false) {
					event.setCancelled(true);
					String name = event.getPlayer().getName();
					if (main.conf.pctocons == true) main.log.info("игрок " + name + " пытался использовать запрещенную команду " + com);
					if (main.conf.pctogame == true) main.sendMes("игрок " + name + " пытался использовать запрещенную команду " + com);
					if (main.conf.gamelog == true) main.sendGlog(name, "пытался использовать запрещенную команду " + com);
					player.sendMessage(main.conf.cpexmess);
			}}}
			
			//иммунитет к командам
			if (main.conf.immunitet == true) {
				String play = ut.cleform(player.getName()).trim();
				String nick = ut.remChars(com, 0, com.lastIndexOf(" ")).trim();
				String command = ut.remChars(com, com.indexOf(" ")+1, com.length()).trim();
				String pworld = player.getWorld().getName().trim();
				boolean imun = ut.chW(nick, ut.mlcase(main.conf.imunele));
				boolean badcom = ut.chW(command, ut.mlcase(main.conf.imcom));
				boolean pw = ut.chW(pworld, main.conf.imworlds);
				if (imun == true && badcom == true && pw == false) {
					event.setCancelled(true);
					if (main.conf.pctocons == true) main.log.info("некто " + play + " пытался выполнить " + com);
					if (main.conf.pctogame == true) main.sendMes("некто " + play + " пытался выполнить " + com);
					if (main.conf.gamelog == true) main.sendGlog(play, "пытался выполнить " + com);
					if (main.conf.imtogad == true) player.sendMessage(main.conf.immessage);
			}}
	}}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void allowCheck(PlayerCommandPreprocessEvent event) { //разрешить команды, если они блокируются
		if (main.conf.allowcom) {
			if (event.isCancelled() && event.getPlayer().hasPermission("allfixer.allowcom")) {
				String com = event.getMessage().trim().toLowerCase();
				for (int i = 0; i < main.conf.allowcoms.length; i++) {
					if (ut.remChars(com, main.conf.allowcoms[i].length(), com.length()).equalsIgnoreCase(main.conf.allowcoms[i])) {
						event.setCancelled(false);
						Player player = event.getPlayer();
						String mir = player.getLocation().getWorld().getName();
						String x = String.valueOf(player.getLocation().getBlockX());
						String y = String.valueOf(player.getLocation().getBlockY());
						String z = String.valueOf(player.getLocation().getBlockZ());
						String loc = mir + " - " + x + " " + y + " " + z;
						if (main.conf.pctocons == true) main.log.info("для" + player.getName() + " была разблокирована команда "  + main.conf.allowcoms[i] + " на " + loc);
						if (main.conf.pctogame == true) main.sendMes("для" + player.getName() + " была разблокирована команда "  + main.conf.allowcoms[i] + " на " + loc);
						break;
					}
				}
			}
		}
	}
}