package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Date;
import java.io.File;
import java.util.UUID;
import java.io.BufferedReader;
import java.io.FileReader;

public class casefix implements Listener {
	private AllFixer main;
	private utils ut = new utils();
	casefix(AllFixer m) {main = m; main.log.info("фикс смены регистра в никах активирован");}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerLogin(AsyncPlayerPreLoginEvent event) { //предотвращение захода с тем же ником в другом регистре
		if (main.conf.casefix) {
			if (!event.getResult().toString().equalsIgnoreCase("ALLOWED")) return;
			
			String name = event.getName();
			
			if (main.conf.esshook) { //прогоняем по БД Essentials, чтобы сократить время поиска
				File map = new File(ut.getPatch() + "/plugins/Essentials/usermap.csv");
				if (map.isFile()) {
					try {
						BufferedReader reader = new BufferedReader(new FileReader(map));
						String line = null; int l = 0;
						do {
							line = reader.readLine();
							l++;
							if (line != null) {
								if (line.indexOf(",") != -1) {
									String oldname = line.split(",")[0];
									if (oldname.equalsIgnoreCase(name)) {
										OfflinePlayer play = Bukkit.getOfflinePlayer(UUID.fromString(line.split(",")[1]));
										if (play != null) {
											oldname = play.getName();
											if (name.equalsIgnoreCase(oldname) & !name.equals(oldname)) {
												event.disallow(Result.valueOf("KICK_OTHER"), main.conf.kickcase.replaceAll("%nick%", oldname));
												if (main.conf.casetocons) main.log.info(oldname + " пытался зайти как " + name);
												if (main.conf.casetogame) main.sendMes(oldname + " пытался зайти как " + name);
												if (main.conf.casetoglog) main.sendGlog(oldname, oldname + " пытался зайти как " + name);
												reader.close();
												return;
											} else {
												reader.close();
												return;
											}
										}
									}
								}
							}
						} while (line != null && l < 2147483647);
						reader.close();
					} catch (Exception e) {e.printStackTrace();}
				}
			}
			
			Date date = new Date();
			long stamp = date.getTime();
			OfflinePlayer plr = Bukkit.getOfflinePlayer(event.getUniqueId());
			long oldstamp = 0;
			if (plr != null) oldstamp = plr.getFirstPlayed(); else return;
			if (oldstamp != 0) {
				long time = (stamp / 1000) - (oldstamp / 1000);
				if (time <= 0 || time > 0 & time < 10) {
					return; //пропускаем новичков
				}
			} else return;
			
			OfflinePlayer pls[] = Bukkit.getOfflinePlayers();
			for (int i = 0; i < pls.length; i++) {
				if (pls[i] != null) {
					String oldname = pls[i].getName();
					if (name.equalsIgnoreCase(oldname) & !name.equals(oldname)) {
						event.disallow(Result.valueOf("KICK_OTHER"), main.conf.kickcase.replaceAll("%nick%", oldname));
						if (main.conf.casetocons) main.log.info(oldname + " пытался зайти как " + name);
						if (main.conf.casetogame) main.sendMes(oldname + " пытался зайти как " + name);
						if (main.conf.casetoglog) main.sendGlog(oldname, oldname + " пытался зайти как " + name);
						break;
					}
				}
			}
		}
	}
}
