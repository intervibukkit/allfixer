package InterVi.AllFixer;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Date;
import java.lang.Thread;

public class fallfix implements Listener {
	AllFixer main;
	private utils ut = new utils("list");
	fallfix(AllFixer all) {main = all; ut.list.setSize(100); main.log.info("фикс падения сквозь блоки активирован");};
	
	private class ycheck extends BukkitRunnable { //вызывается через заданный промежуток и проверяет, не упал ли игрок
		Player player; Location to;
		ycheck(Player p, Location tto) {this.player = p; this.to = tto;}
		public void run() {
			if (player != null && player.isOnline() & main.conf.timercheck) {
				this.player = Bukkit.getPlayer(player.getName()); //обновляем данные
				if (player != null) {
				Location ploc = player.getLocation();
				int y2 = ploc.getBlockY();
				int y = to.getBlockY();
				if ((y-y2) >= main.conf.fallblocks | !ploc.getBlock().isEmpty() && to.getWorld().isChunkLoaded(to.getBlockX(), to.getBlockZ())) {
					tp tpp = new tp();
					tpp.go(player, to);
					this.cancel();
	}}}}}
	
	private class wait extends Thread { //класс ожидания телепорта после прогрузки чанка
		Player player; Location to;
		wait(Player p, Location t) {
			player = p;
			to = t;
		}
		
		@Override
		public void run() {
			if (player != null && to != null && main.conf.loadwait) {
				for (int i = 0; i < 20000; i++) {
					int x = to.getBlockX(); int z = to.getBlockZ();
					if (to.getWorld().isChunkLoaded(x, z)) {
						this.player = Bukkit.getPlayer(player.getName()); //обновляем данные
						if (player != null) {
							tp tp = new tp();
							tp.go(player, to);
						}
						break;
					}
					try {
						Thread.sleep(100);
					} catch (Exception e) {e.printStackTrace();}
				}
			}
		}
	}
	
	private class tp extends BukkitRunnable { //класс для осуществления телепортации
		Player player = null; Location to = null;
		void go(Player play, Location loc) {
			player = play;
			to = loc;
			this.runTask(Bukkit.getPluginManager().getPlugin("AllFixer"));
		}
		
		@Override
		public void run() {
			if (player != null && to != null) player.teleport(to);
			this.cancel();
		}
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void PlayerTp(PlayerTeleportEvent event) {
		if (main.conf.fallfix == true && event.getPlayer() != null && !event.isCancelled()) { //комплекс мер против падения сквозь блоки
			Player player = event.getPlayer();
			Location to = event.getTo();
			String mir3 = player.getLocation().getWorld().getName();
			int x3 = player.getLocation().getBlockX();
			int z3 = player.getLocation().getBlockZ();
			String str = mir3 + x3 + z3;
			
			int x2 = to.getBlockX();
			int y2 = to.getBlockY();
			int z2 = to.getBlockZ();
			
			Date date = new Date();
			long stamp = date.getTime();
			long check = -1;
			boolean yes = true;
				String ch[] = ut.list.getData();
				if (ch != null) {
					for (int i = 0; i < ch.length; i++) {
						if (ch[i] != null) {
							if (ch[i].indexOf(";") != -1) {
								if (ch[i].split(";")[0].equals(str)) {
									try {
										check = Long.parseLong(ch[i].split(";")[1]);
									} catch (Exception e) {e.printStackTrace();}
									if (check > -1) {
										long t = (stamp / 1000) - (check / 1000);
										if (t <= 10) {
											yes = false; //таймаут на проверку чанка в 10сек, чтобы избежать краша
										}
									}
									break;
								}
							}
						}
					}
				}
			
			if (player.getLocation().getWorld().equals(to.getWorld())) {if (player.getLocation().distance(to) <= 50) yes = false;} //ближние телепорты не обслуживаем
			
			if (yes) { //антиглюк (спасает на 95%)
			if (main.conf.preload == true) { //прогрузка чанка
				if (to.getWorld().isChunkLoaded(x2, z2) == false) {
					try { //антикраш
						to.getWorld().loadChunk(x2, z2);
					} catch (Exception e) {}
					if (main.conf.loadwait) { //ожидание прогрузки
						event.setCancelled(true);
						wait w = new wait(player, to);
						w.start();
						yes = false;
					}
				}
			}
			
			if (main.conf.checkair == true) { //проверка на застревание в блоках
				boolean air = false;
				if (player.getLocation().getBlock().isEmpty() == false) { //проверка на безобидные блоки вроде травы чтобы небыло бесполезных срабатываний
					int block = player.getLocation().getBlock().getTypeId();
					for (int i = 0; i < main.conf.nofall.length; i++) {
						if (block == main.conf.nofall[i]) {air = true; break;} else if (i == main.conf.nofall.length - 1) air = false; 
					}
				} else {air = true;};
				if (air == false) {
					String mir = to.getWorld().getName();
					double xx = x2;
					double yy = y2;
					double zz = z2;
					if (ut.chW(mir, main.conf.nofallw) == false) {
						Location locc = new Location(to.getWorld(), xx, yy, zz);
						for (int i = 0; locc.getBlock().isEmpty() == false; i++, yy++) {locc.setY(yy); if (i > main.conf.maxblocks) break;};
						event.setTo(locc);
					}
				}
			}
			if (((stamp / 1000) - (player.getLastPlayed() / 1000)) <= main.conf.noftimer) yes = false;
			if (main.conf.timercheck == true && yes && !event.isCancelled()) { //проверка на падение через заданное время
				String str3 = to.getWorld().getName() + to.getBlockX() + to.getBlockZ();
				ut.list.send((str3 + ";" + String.valueOf(stamp))); //антиглюк
				ycheck ych = new ycheck(player, to);
				ych.runTaskLater(Bukkit.getPluginManager().getPlugin("AllFixer"), main.conf.falltimer);
			}
			if (!event.isCancelled() && yes) ut.list.send((str + ";" + String.valueOf(stamp))); //антиглюк
	}}}
}