package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.entity.Player;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class fixmute implements Listener {
	AllFixer main;
	private utils ut = new utils("list");
	fixmute(AllFixer all) {main = all; ut.list.setSize(main.conf.mutsize); main.log.info("фикс mute активирован");};
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void ComSend(PlayerCommandPreprocessEvent event) {
		if (main.conf.fixmute == true && event.getPlayer() != null) {
			Player player = event.getPlayer();
			String com = event.getMessage().toLowerCase();
			
			boolean check = false;
			for (int i = 0; i < main.conf.mutcom.length; i++) { //проверка, относится ли команда к командам из списка
				String ch = ut.remChars(com, main.conf.mutcom[i].length(), com.length());
				if (ch.equals(main.conf.mutcom[i])) {
					check = true;
					break;
				} else check = false;
			}
			
			if (check == true && player.isOp() == false && player.hasPermission("allfixer.nomute") == false && check(player.getDisplayName()) == true) {
				event.setCancelled(true);
				player.sendMessage(main.conf.mutmess);
			}
		}
	}
	
	public void send(String s) { //отправка ника в базу (выдача мута)
		s = ut.cleform(s); //чистка форматирования
		ut.list.send(s.toLowerCase());
	}
	
	public void rem(String s) { //удаление ника из базы (размут)
		s = ut.cleform(s); //чистка форматирования
		ut.list.del(s.toLowerCase());
	}
	
	private boolean check(String p) { //проверка, есть ли ник в базе
		boolean result = false;
		String mas[] = new String[ut.list.getData().length];
		mas = ut.list.getData();
		p = p.toLowerCase();
		p = ut.cleform(p); //чистка форматирования
		for (int i = 0; i < mas.length; i++) {
			if (p.equals(mas[i])) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	public String getMuted() { //получение списка замученных строкой
		return ut.list.getDataString();
	}
	
	class timer {
		/*
		 * класс с методами для определения времени
		 * массив int с тайм-штампами (секунды)
		 * каждую секунду таймер делает всем -1
		 * когда штамп доходит до 0 - игрок размучивается
		 */
	}
}