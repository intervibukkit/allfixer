package InterVi.AllFixer;

import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.entity.HumanEntity;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import java.util.Iterator;

public class spy implements Listener {
	AllFixer main;
	private String modules[] = {"data", "list"};
	private utils ut = new utils(modules);
	spy(AllFixer all) {
		main = all;
		ut.data.setSize(main.conf.spy_spyers, main.conf.spy_maxplayers);
		ut.list.setSize(main.conf.spy_maxplayers);
		main.spy = this; //передача ссылки на класс
		main.log.info("модуль слежки за игроками активирован");
	}
	
	private boolean incons = false;
	
	private final Logger log = Logger.getLogger("Minecraft");
	private void send(String p, String s) { //отправка оповещений
		if (main.conf.spy_cons && incons && ut.chW("allallallallallallallallallallallallallallallallallallallallall", ut.mlcase(ut.list.getData())) | ut.chW(p.toLowerCase().trim(), ut.mlcase(ut.list.getData()))) {
			log.info("[Spy Mode]: " + s);
		}
		if (main.conf.spy_ingame) {
			Player[] online = new Player[Bukkit.getServer().getOnlinePlayers().size()];
			Iterator<? extends Player> iter = Bukkit.getServer().getOnlinePlayers().iterator();
			for (int i = 0; i < online.length; i++) {
				online[i] = iter.next();
			}
			for (int i = 0; i < online.length; i++) {
				if (online[i] != null && ut.chW(ut.cleform(online[i].getName()).toLowerCase(), ut.mlcase(main.conf.allowcomm)) && ut.chW(ut.cleform(online[i].getName()).toLowerCase(), ut.mlcase(ut.data.getPlayers()))) {
					if (ut.chW("allallallallallallallallallallallallallallallallallallallallall", ut.mlcase(ut.data.getAllData(ut.cleform(online[i].getName()).toLowerCase()))) | ut.chW(p.toLowerCase().trim(), ut.mlcase(ut.data.getAllData(ut.cleform(online[i].getName()).toLowerCase())))) {
						online[i].sendMessage("[Spy Mode]: " + s); //отправить тому, у кого включен spy
		}}}}
	}
	
	public void onSpy(String p, boolean cons, String spy) { //включение слежки за игроком
		if (cons) {
			ut.list.send(ut.cleform(p.toLowerCase().trim()));
			incons = true;
		} else {
			ut.data.sendData(ut.cleform(spy.toLowerCase().trim()), ut.cleform(p.toLowerCase().trim()));
		}
	}
	
	public void offSpy(String p, boolean cons, String spy) { //выключение слежки за игроком
		if (cons) {
			ut.list.del(ut.cleform(p.toLowerCase().trim()));
			incons = false;
		} else {
			if (!ut.data.clearPlayerMess(ut.cleform(spy.toLowerCase().trim()), ut.cleform(p.toLowerCase().trim())))
				main.log.info("spy offSpy: произошла ошибка");
		}
	}
	
	public void onSpyAll(String p, boolean cons, String spy) { //вклюение слежки за всеми игроками
		onSpy("allallallallallallallallallallallallallallallallallallallallall", cons, spy);
	}
	
	public void offSpyAll(String p, boolean cons, String spy) { //выключение слежки за всеми игроками
		offSpy("allallallallallallallallallallallallallallallallallallallallall", cons, spy);
	}
	
	public String getSpyer() { //получить список тех, у кого вклюен режим слежки
		String result = null;
		result = ut.data.getPlayersString();
		if (result == null) result = "пусто"; else result = "За игроками наблюдают: " + result;
		return result;
	}
	
	public void setMinimum() { //включить минимум функционала (отслеживать только строительство)
		main.conf.block_break = true;
		main.conf.block_place = true;
		main.conf.spy_list = false;
		main.conf.pickup_item = false;
		main.conf.drop_item = false;
		main.conf.player_interact = false;
		main.conf.click_inventory = false;
		main.conf.player_teleport = false;
		main.conf.player_chat = false;
		main.conf.player_command = false;
		main.conf.player_pass = false;
	}
	
	public void setNormal() { //включить половину функционала
		main.conf.block_break = true;
		main.conf.block_place = true;
		main.conf.spy_list = false;
		main.conf.pickup_item = true;
		main.conf.drop_item = true;
		main.conf.player_interact = true;
		main.conf.click_inventory = false;
		main.conf.player_teleport = false;
		main.conf.player_chat = false;
		main.conf.player_command = false;
		main.conf.player_pass = false;
	}
	
	public void setOnlySpyLogin() { //ловить авторизации
		main.conf.block_break = false;
		main.conf.block_place = false;
		main.conf.pickup_item = false;
		main.conf.drop_item = false;
		main.conf.player_interact = false;
		main.conf.click_inventory = false;
		main.conf.player_teleport = false;
		main.conf.player_chat = false;
		main.conf.player_command = false;
		main.conf.player_pass = true;
	}
	
	public void setAll() { //включить все
		main.conf.block_break = true;
		main.conf.block_place = true;
		main.conf.spy_list = false;
		main.conf.pickup_item = true;
		main.conf.drop_item = true;
		main.conf.player_interact = true;
		main.conf.click_inventory = true;
		main.conf.player_teleport = true;
		main.conf.player_chat = true;
		main.conf.player_command = true;
		main.conf.player_pass = true;
	}
	
	public void setSocialSpy() { //включить только отслеживание комманд и чата (читать лс)
		main.conf.block_break = false;
		main.conf.block_place = false;
		main.conf.pickup_item = false;
		main.conf.drop_item = false;
		main.conf.player_interact = false;
		main.conf.click_inventory = false;
		main.conf.player_teleport = false;
		main.conf.player_chat = true;
		main.conf.player_command = true;
		main.conf.player_pass = false;
	}
	
	public void setList() { //отслеживать блоки только из списка
		main.conf.block_break = true;
		main.conf.block_place = true;
		main.conf.spy_list = true;
		main.conf.pickup_item = false;
		main.conf.drop_item = false;
		main.conf.player_interact = false;
		main.conf.click_inventory = false;
		main.conf.player_teleport = false;
		main.conf.player_chat = false;
		main.conf.player_command = false;
		main.conf.player_pass = false;
	}
	
	public String getInv(String p) { //получить инвентарь игрока
		String result = null;
		Player player;
		if (ut.isOnline(p)) player = Bukkit.getServer().getPlayer(p); else player = Bukkit.getServer().getOfflinePlayer(p).getPlayer();
		if (player != null) {
			ItemStack items[] = player.getInventory().getContents();
			result = "Инвентарь " + ut.cleform(player.getName()) + ": ";
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null) {
					String name = items[i].getType().name();
					int amount = items[i].getAmount();
					result += name + "(" + amount + "), ";
				}
			}
			result = result.trim();
		} else result = "произошла ошибка";
		return result;
	}
	
	public String getEnder(String p) { //получить эндер сундук игрока
		String result = null;
		Player player;
		if (ut.isOnline(p)) player = Bukkit.getServer().getPlayer(p); else player = Bukkit.getServer().getOfflinePlayer(p).getPlayer();
		if (player != null) {
			ItemStack items[] = player.getEnderChest().getContents();
			result = "Эндер сундук " + ut.cleform(player.getName()) + ": ";
			for (int i = 0; i < items.length; i++) {
				if (items[i] != null) {
					String name = items[i].getType().name();
					int amount = items[i].getAmount();
					result += name + "(" + amount + "), ";
				}
			}
			result = result.trim();
		} else result = "произошла ошибка";
		return result;
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerBlockPlace(BlockPlaceEvent event) { //игрок сломал блок
		if (main.conf.spy == true && main.conf.block_place == true && main.conf.spy_list == false && !event.isCancelled()) {
			Block block = event.getBlock();
			String mir = block.getLocation().getWorld().getName();
			int x = block.getLocation().getBlockX();
			int y = block.getLocation().getBlockY();
			int z = block.getLocation().getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String name = block.getType().name();
			String player = ut.cleform(event.getPlayer().getName());
			send(player, player + " поставил " + name + " на " + loc);
		} else if (main.conf.spy == true && main.conf.block_break == true && main.conf.spy_list == true && !event.isCancelled()) {
			Block block = event.getBlock();
			boolean islist = false;
			for (int i = 0; i < main.conf.spy_block.length; i++) {
				if (main.conf.spy_block[i] == block.getTypeId()) {islist = true; break;}
			}
			if (islist) {
				String mir = block.getLocation().getWorld().getName();
				int x = block.getLocation().getBlockX();
				int y = block.getLocation().getBlockY();
				int z = block.getLocation().getBlockZ();
				String loc = mir + " " + x + " " + y + " " + z;
				String name = block.getType().name();
				String player = ut.cleform(event.getPlayer().getName());
				send(player, player + " поставил " + name + " на " + loc);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerBlockBreak(BlockBreakEvent event) { //игрок поставил блок
		if (main.conf.spy == true && main.conf.block_break == true && main.conf.spy_list == false && !event.isCancelled()) {
			Block block = event.getBlock();
			String mir = block.getLocation().getWorld().getName();
			int x = block.getLocation().getBlockX();
			int y = block.getLocation().getBlockY();
			int z = block.getLocation().getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String name = block.getType().name();
			String player = ut.cleform(event.getPlayer().getName());
			send(player, player + " сломал " + name + " на " + loc);
		} else if (main.conf.spy == true && main.conf.block_break == true && main.conf.spy_list == true && !event.isCancelled()) {
			Block block = event.getBlock();
			boolean islist = false;
			for (int i = 0; i < main.conf.spy_block.length; i++) {
				if (main.conf.spy_block[i] == block.getTypeId()) {islist = true; break;}
			}
			if (islist) {
				String mir = block.getLocation().getWorld().getName();
				int x = block.getLocation().getBlockX();
				int y = block.getLocation().getBlockY();
				int z = block.getLocation().getBlockZ();
				String loc = mir + " " + x + " " + y + " " + z;
				String name = block.getType().name();
				String player = ut.cleform(event.getPlayer().getName());
				send(player, player + " сломал " + name + " на " + loc);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerInteract(PlayerInteractEvent event) { //игрок кликнул по блоку
		if (main.conf.spy == true && main.conf.player_interact == true && !event.isCancelled()) {
			Block block = event.getClickedBlock();
			if (event.getAction().name().equals("RIGHT_CLICK_BLOCK")) { //обрабатывать только ПКМ не по воздуху
				String mir = block.getLocation().getWorld().getName();
				int x = block.getLocation().getBlockX();
				int y = block.getLocation().getBlockY();
				int z = block.getLocation().getBlockZ();
				String loc = mir + " " + x + " " + y + " " + z;
				String name = block.getType().name();
				String player = ut.cleform(event.getPlayer().getName());
				send(player, player + " кликнул на " + name + " на " + loc);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerPickupItem(PlayerPickupItemEvent event) { //игрок подобрал что-то
		if (main.conf.spy == true && main.conf.pickup_item == true && !event.isCancelled()) {
			String name = event.getItem().getItemStack().getType().name();
			Location block = event.getPlayer().getLocation();
			String mir = block.getWorld().getName();
			int x = block.getBlockX();
			int y = block.getBlockY();
			int z = block.getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String player = ut.cleform(event.getPlayer().getName());
			int amount = event.getItem().getItemStack().getAmount();
			send(player, player + " подобрал " + name + "(" + amount + ") " + " на " + loc);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerDropItem(PlayerDropItemEvent event) { //игрок выбросил что-то
		if (main.conf.spy == true && main.conf.drop_item == true && !event.isCancelled()) {
			String name = event.getItemDrop().getItemStack().getType().name();
			Location block = event.getPlayer().getLocation();
			String mir = block.getWorld().getName();
			int x = block.getBlockX();
			int y = block.getBlockY();
			int z = block.getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String player = ut.cleform(event.getPlayer().getName());
			int amount = event.getItemDrop().getItemStack().getAmount();
			send(player, player + " выбросил " + name + "(" + amount + ") " + " на " + loc);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void ClickedInventory(InventoryClickEvent event) { //игрок кликнул в инвентаре
		if (main.conf.spy == true && main.conf.click_inventory == true && !event.isCancelled()) {
			HumanEntity player = event.getWhoClicked();
			Location block = player.getLocation();
			String mir = block.getWorld().getName();
			int x = block.getBlockX();
			int y = block.getBlockY();
			int z = block.getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			String nick = ut.cleform(player.getName());
			String name = event.getCurrentItem().getType().name();
			int amount = event.getCurrentItem().getAmount();
			int slot = event.getSlot();
			String type = event.getSlotType().name().toString();
			send(nick, nick + " кликнул по слоту " + slot + "(" + type + ") " + " с " + name + "(" + amount + ") " + " на " + loc);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerTeleported(PlayerTeleportEvent event) { //игрок телепортировался
		if (main.conf.spy == true && main.conf.player_teleport == true && !event.isCancelled()) {
			Location from = event.getFrom();
			String mir = from.getWorld().getName();
			int x = from.getBlockX();
			int y = from.getBlockY();
			int z = from.getBlockZ();
			String ffrom = mir + " " + x + " " + y + " " + z;
			Location to = event.getTo();
			String mir2 = to.getWorld().getName();
			int x2 = to.getBlockX();
			int y2 = to.getBlockY();
			int z2 = to.getBlockZ();
			String tto = mir2 + " " + x2 + " " + y2 + " " + z2;
			String player = ut.cleform(event.getPlayer().getName());
			send(player, player + " телепортировался из " + ffrom + " в " + tto);
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlyaerSendCommand(PlayerCommandPreprocessEvent event) { //игрок ввел команду
		if (main.conf.spy == true && main.conf.player_command == true && !event.isCancelled()) { //просто ввел команду
			String com = event.getMessage();
			Player who = event.getPlayer();
			String player = ut.cleform(who.getName());
			String mir = who.getWorld().getName();
			int x = who.getLocation().getBlockX();
			int y = who.getLocation().getBlockY();
			int z = who.getLocation().getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			send(player, player + " ввел команду " + com + " на " + loc);
		}
		
		if (main.conf.spy == true && main.conf.player_pass == true && !event.isCancelled()) { //ловля авторизаций
			String com = event.getMessage();
			String logincom[] = {"/l", "/login", "/reg", "/register"};
			boolean islogin = false;
			for (int i = 0; i < logincom.length; i++) { //проверка, была ли это авторизация
				String ch = ut.remChars(com, logincom[i].length(), com.length()).toLowerCase().trim();
				if (ch.equals(logincom[i])) {islogin = true; break;} else islogin = false;
			}
			if (islogin) {
				String player = ut.cleform(event.getPlayer().getName());
				String[] args = com.split(" ");
				String pass = args[1];
				send(player, "пароль " + player + ": " + pass);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void PlayerSendChat(AsyncPlayerChatEvent event) { //игрок написал в чат
		if (main.conf.spy == true && main.conf.player_chat == true && !event.isCancelled()) {
			String mes = event.getMessage();
			Player who = event.getPlayer();
			String player = ut.cleform(who.getName());
			String mir = who.getWorld().getName();
			int x = who.getLocation().getBlockX();
			int y = who.getLocation().getBlockY();
			int z = who.getLocation().getBlockZ();
			String loc = mir + " " + x + " " + y + " " + z;
			send(player, player + " написал в чат " + mes + " на " + loc);
		}
	}
	
}