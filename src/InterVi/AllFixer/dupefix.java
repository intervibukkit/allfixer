package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPortalEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.entity.Player;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.event.block.BlockPhysicsEvent;
import org.bukkit.entity.Horse;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.block.Block;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.block.BlockExplodeEvent;
import java.util.Date;

import InterVi.AllFixer.utils.memory;

public class dupefix implements Listener {
	AllFixer main;
	private String utar[] = {"mem", "list"};
	private utils ut = new utils(utar);
	private memory mem2 = ut.mem.getMemory();
	private memory mem3 = ut.mem.getMemory();
	dupefix(AllFixer all) {main = all; ut.mem.setSize(3000); mem2.setSize(200); mem3.setSize(10); ut.list.setSize(10); main.log.info("фикс дюпов активирован");};
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void minecartTp(EntityPortalEvent event) {
		if (main.conf.dupefix == true && main.conf.nportal == true) {
			String type = event.getEntityType().toString();
			if (type.equalsIgnoreCase("MINECART_CHEST") | type.equalsIgnoreCase("MINECART_HOPPER") | type.equalsIgnoreCase("MINECART_FURNACE")) { //дюп с отправкой грузовой вагонетки в портал
				event.setCancelled(true); //отменяем телепорт
				String mir = event.getFrom().getWorld().getName();
				int x = event.getFrom().getBlockX();
				int y = event.getFrom().getBlockY();
				int z = event.getFrom().getBlockZ();
				if (main.conf.console == true && main.conf.duptocons == true) main.log.info("попытка дюпа вагонеткой на " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.mesingame == true && main.conf.duptogame == true) main.sendMes("попытка дюпа вагонеткой на " + mir + " - " + x + " " + y + " " + z);
				if (main.conf.gamelog == true) main.sendGlog(null, "попытка дюпа вагонеткой на " + mir + " - " + x + " " + y + " " + z);
			}
			if (type.equalsIgnoreCase("HORSE")) { //дюп с отправкой осла в портал
				String var = ((Horse) event.getEntity()).getVariant().toString();
				if (var.equalsIgnoreCase("DONKEY") | var.equalsIgnoreCase("MULE")) {
					event.setCancelled(true); //отменяем телепорт
					String mir = event.getFrom().getWorld().getName();
					int x = event.getFrom().getBlockX();
					int y = event.getFrom().getBlockY();
					int z = event.getFrom().getBlockZ();
					String loc = mir + " - " + x + " " + y + " " + z;
					if (main.conf.console == true && main.conf.duptocons == true) main.log.info("попытка дюпа ослом на " + loc);
					if (main.conf.mesingame == true && main.conf.duptogame == true) main.sendMes("попытка дюпа ослом на " + loc);
					if (main.conf.gamelog == true) main.sendGlog(null, "попытка дюпа ослом на " + loc);
				}
			}
	}
	}
	
	//дюп растений с помощью горшка
	@EventHandler(priority = EventPriority.HIGHEST)
	public void blockBreak(BlockBreakEvent event) { //когда игрок ломает блок
		if (main.conf.dupefix == true && main.conf.bpot == true && !event.isCancelled()) {
			Player player = event.getPlayer();
			if (player != null && player.isOp() == false & !player.hasPermission("allfixer.allowdupe")) {
				//координаты игрока
				String mir = player.getLocation().getWorld().getName();
				int x = player.getLocation().getBlockX();
				int y = player.getLocation().getBlockY();
				int z = player.getLocation().getBlockZ();
				String loc = mir + " - " + x + " " + y + " " + z;
				//координаты блока
				double xx = event.getBlock().getX();
				double yy = event.getBlock().getY();
				double zz = event.getBlock().getZ();
				World world = event.getBlock().getWorld();
				//вычисление блока на блок выше
				Location locc = new Location(world, xx, yy + 1, zz);
				int block = locc.getBlock().getTypeId();
				//проверка
				if (block == 140) {
					event.setCancelled(true);
					if (main.conf.console == true && main.conf.duptocons == true && main.conf.dupepot == true) main.log.info("игрок " + player.getName() + " пытался дюпнуть горшком растения на " + loc);
					if (main.conf.mesingame == true && main.conf.duptogame == true && main.conf.dupepot == true) main.sendMes("игрок " + player.getName() + " пытался дюпнуть горшком растения на " + loc);
					if (main.conf.gamelog == true) main.sendGlog(player.getName(), "пытался дюпнуть горшком растения на " + loc);
	}}}}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void blockPhyzic(BlockPhysicsEvent event) { //обработка физики (от хитростей сдвигом поршнем и т.д.)
		if (main.conf.dupefix == true && main.conf.bpot == true && !event.isCancelled()) {
			if (event.getBlock().getTypeId() == 140) {
				event.setCancelled(true);
				int x = event.getBlock().getX();
				int y = event.getBlock().getY();
				int z = event.getBlock().getZ();
				String mir = event.getBlock().getWorld().getName();
				String loc = mir + " - " + x + " " + y + " " + z;
				if (main.conf.console == true && main.conf.duptocons == true && ut.mem.isVal(loc.hashCode()) == false && main.conf.dupepot == true) main.log.info("попытка дюпнуть горшком растения на " + loc);
				if (main.conf.mesingame == true && main.conf.duptogame == true && ut.mem.isVal(loc.hashCode()) == false && main.conf.dupepot == true) main.sendMes("попытка дюпнуть горшком растения на " + loc);
				if (main.conf.gamelog == true && ut.mem.isVal(loc.hashCode()) == false) main.sendGlog(null, "попытка дюпнуть горшком растения на " + loc);
				ut.mem.sendValue(loc.hashCode()); // защита от флуда
				if (ut.mem.colVal(loc.hashCode()) >= 20) ut.mem.delVal(loc.hashCode()); //вывод каждого 20-ого одинакового сообщения
	}}}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInteract(PlayerInteractEvent event) { //фикс дюпа с застреванием в вагонетке (клик по блоку)
		if (main.conf.dupefix && main.conf.dupsit && event.getPlayer() != null & event.getClickedBlock() != null  && !event.isCancelled()) {
			Player player = event.getPlayer();
			if (player.isInsideVehicle() && !player.isOp() & !player.hasPermission("allfixer.allowdupe")) {
				Block block = event.getClickedBlock();
				int id = block.getTypeId();
				boolean banned = false;
				for (int i = 0; i < main.conf.nositint.length; i++) {
					if (id == main.conf.nositint[i]) {banned = true; break;}
				}
				if (banned) {
					event.setCancelled(true);
					player.sendMessage("нельзя взаимодействовать с данным блоком сидя");
					String mir = player.getLocation().getWorld().getName();
					int x = player.getLocation().getBlockX();
					int y = player.getLocation().getBlockY();
					int z = player.getLocation().getBlockZ();
					String name = player.getName();
					String loc = mir + " - " + x + " " + y + " " + z;
					String block2 = block.getType().toString();
					if (main.conf.console == true && main.conf.duptocons == true && main.conf.dupepot == true) main.log.info("игрок " + name + " пытался взаимодействовать с блоком " + block2 + " сидя (дюп), на " + loc);
					if (main.conf.mesingame == true && main.conf.duptogame == true && main.conf.dupepot == true) main.sendMes("игрок " + name + " пытался взаимодействовать с блоком " + block2 + " сидя (дюп), на " + loc);
					if (main.conf.gamelog == true) main.sendGlog(name, "пытался взаимодействовать с блоком " + block2 + " сидя (дюп), на " + loc);
	}}}}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onInteractEntity(PlayerInteractEntityEvent event) { //фикс с застреванием в вагонетке (клик по сущности)
		if (main.conf.dupefix && main.conf.dupsit && event.getPlayer() != null && !event.isCancelled()) {
			Player player = event.getPlayer();
			String type = event.getRightClicked().getType().toString();
			if (player.isInsideVehicle() & !player.isOp() & !player.hasPermission("allfixer.allowdupe") && type.equalsIgnoreCase("HORSE")) {
				String htype = ((Horse) event.getRightClicked()).getVariant().toString();
				if (htype.equalsIgnoreCase("DONKEY") | htype.equalsIgnoreCase("MULE")) {
				event.setCancelled(true);
				player.sendMessage("нельзя взаимодействовать с ослом сидя");
				String mir = player.getLocation().getWorld().getName();
				int x = player.getLocation().getBlockX();
				int y = player.getLocation().getBlockY();
				int z = player.getLocation().getBlockZ();
				String name = player.getName();
				String loc = mir + " - " + x + " " + y + " " + z;
				if (main.conf.console == true && main.conf.duptocons == true && main.conf.dupepot == true) main.log.info("игрок " + name + " пытался взаимодействовать с ослом сидя (дюп), на " + loc);
				if (main.conf.mesingame == true && main.conf.duptogame == true && main.conf.dupepot == true) main.sendMes("игрок " + name + " пытался взаимодействовать с ослом сидя (дюп), на " + loc);
				if (main.conf.gamelog == true) main.sendGlog(name, "пытался взаимодействовать с ослом сидя (дюп), на " + loc);
	}}}}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExplosionEntity(EntityExplodeEvent event) { //фикс дюпа со взрывом сундука (сущностью)
		if (main.conf.dupefix && main.conf.exchest && !event.isCancelled()) {
			int b = event.blockList().size();
			Block block[] = new Block[b];
			for (int i = 0; i < b; i++) { //заполняем массив
				block[i] = event.blockList().get(i);
			}
			for (int i = 0; i < block.length; i++) {
				if (block[i] != null) {
					for (int i2 = 0; i2 < main.conf.remtiles.length; i2++) {
						if (block[i].getTypeId() == main.conf.remtiles[i2]) { //если пытались взорвать запрещенный блок
							String block2 = block[i].getType().toString();
							block[i].setTypeId(0); //заменяем его воздухом
							Location loc = event.getEntity().getLocation();
							String mir = loc.getWorld().getName();
							int x = loc.getBlockX();
							int y = loc.getBlockY();
							int z = loc.getBlockZ();
							String locc = mir + " - " + x + " " + y + " " + z;
							int hash = locc.hashCode();
							if (!mem2.isVal(hash)) { //антифлуд
								if (main.conf.console == true && main.conf.duptocons == true) main.log.info("пытались взорвать " + block2 + " (дюп), на " + locc);
								if (main.conf.mesingame == true && main.conf.duptogame == true) main.sendMes("пытались взорвать " + block2 + " (дюп), на " + locc);
								if (main.conf.gamelog == true) main.sendGlog(null, "пытались взорвать " + block2 + " (дюп), на " + locc);
								mem2.sendValue(hash);
								if (mem2.colVal(hash) >= 20) mem2.delVal(hash);
	}}}}}}}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExplosionBlock(BlockExplodeEvent event) { //фикс дюпа со взрывом сундука (блоком)
		if (main.conf.dupefix && main.conf.exchest && !event.isCancelled()) {
			int b = event.blockList().size();
			Block block[] = new Block[b];
			for (int i = 0; i < b; i++) { //заполняем массив
				block[i] = event.blockList().get(i);
			}
			for (int i = 0; i < block.length; i++) {
				if (block[i] != null) {
					for (int i2 = 0; i2 < main.conf.remtiles.length; i2++) {
						if (block[i].getTypeId() == main.conf.remtiles[i2]) { //если пытались взорвать запрещенный блок
							String block2 = block[i].getType().toString();
							block[i].setTypeId(0); //заменяем его воздухом
							Location loc = event.getBlock().getLocation();
							String mir = loc.getWorld().getName();
							int x = loc.getBlockX();
							int y = loc.getBlockY();
							int z = loc.getBlockZ();
							String locc = mir + " - " + x + " " + y + " " + z;
							int hash = locc.hashCode();
							if (!mem2.isVal(hash)) { //антифлуд
								if (main.conf.console == true && main.conf.duptocons == true) main.log.info("пытались взорвать " + block2 + " (дюп), на " + locc);
								if (main.conf.mesingame == true && main.conf.duptogame == true) main.sendMes("пытались взорвать " + block2 + " (дюп), на " + locc);
								if (main.conf.gamelog == true) main.sendGlog(null, "пытались взорвать " + block2 + " (дюп), на " + locc);
								mem2.sendValue(hash);
								if (mem2.colVal(hash) >= 20) mem2.delVal(hash);
	}}}}}}}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onClickInInv(InventoryClickEvent event) {
		if (main.conf.dupefix && main.conf.reminf && event.getCurrentItem() != null) { //фикс дюпа с бесконечными предметами
			Player player = (Player) event.getWhoClicked();
			ItemStack inv[] = player.getInventory().getContents();
			if (!player.isOp() && !player.hasPermission("allfixer.allowdupe")) {
			for (int i = 0; i < inv.length; i++) {
			if (inv[i] != null) {
			int amount = inv[i].getAmount();
			if (amount <= 0) { //если предмет багнутый
				player.getInventory().clear(i); //удаляем его
				String itemm = inv[i].getType().toString();
				String mir = player.getLocation().getWorld().getName();
				int x = player.getLocation().getBlockX();
				int y = player.getLocation().getBlockY();
				int z = player.getLocation().getBlockZ();
				String name = player.getName();
				String loc = mir + " - " + x + " " + y + " " + z;
				if (main.conf.console == true && main.conf.duptocons == true) main.log.info("у игрока " + name + " нашелся багнутый предмет " + itemm + " (дюп), на " + loc);
				if (main.conf.mesingame == true && main.conf.duptogame == true) main.sendMes("у игрока " + name + " нашелся багнутый предмет " + itemm + " (дюп), на " + loc);
				if (main.conf.gamelog == true) main.sendGlog(name, "нашелся багнутый предмет " + itemm + " (дюп), на " + loc);
	}}}}}
	if (main.conf.dupefix && main.conf.fastclick && event.getCurrentItem() != null) { //блокировать быстрые клики
		Player player = (Player) event.getWhoClicked();
		String type = event.getSlotType().toString();
		if (!player.isOp() && !player.hasPermission("allfixer.allowdupe") && event.getClick().isLeftClick() | event.getClick().isRightClick() && !type.equalsIgnoreCase("CONTAINER") | !type.equalsIgnoreCase("FUEL")) {
			Date date = new Date();
			long stamp = date.getTime();
			String name = player.getName();
			String str = name + ";" + String.valueOf(stamp) + ";" + String.valueOf(event.getSlot());
			ut.list.send(str);
			String arr[] = ut.list.getData();
			int c = 0; int s = 0;
			for (int i = 0; i < arr.length; i++) {
				if (arr[i].split(";")[0].equals(name)) {
					long oldstamp = 0;
					try {
						oldstamp = Long.parseLong(arr[i].split(";")[1]);
					} catch (Exception e) {e.printStackTrace();}
					long ch = (stamp / 1000) - (oldstamp / 1000);
					if (ch <= main.conf.maxtimecl) c++;
					if (i > 0) {
						if (arr[i].split(";")[2].equals(arr[(i-1)].split(";")[2])) s++;
					}
				}
			}
			if (c > main.conf.maxclicks && c == s) {
				event.setCancelled(true);
				player.closeInventory();
				int hash = name.hashCode();
				if (!mem3.isVal(hash)) { //антифлуд
					String mir = player.getLocation().getWorld().getName();
					int x = player.getLocation().getBlockX();
					int y = player.getLocation().getBlockY();
					int z = player.getLocation().getBlockZ();
					String loc = mir + " - " + x + " " + y + " " + z;
					if (main.conf.console == true && main.conf.duptocons == true) main.log.info("игрок " + name + " пытался быстро кликать (дюп), на " + loc);
					if (main.conf.mesingame == true && main.conf.duptogame == true) main.sendMes("игрок " + name + " пытался быстро кликать (дюп), на " + loc);
					if (main.conf.gamelog == true) main.sendGlog(name, "пытался быстро кликать (дюп), на " + loc);
					mem3.sendValue(hash);
				}
			}
		}
	}
	}
}