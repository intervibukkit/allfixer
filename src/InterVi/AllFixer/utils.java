package InterVi.AllFixer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.FileWriter;
import java.io.BufferedWriter;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class utils {
	utils() {}; //инициализируются только методы
	memory mem2;
	utils(int val) {memory memus = new memory(val); mem2 = memus;}; //инициализируется memory с заданным значением
	memory mem; count count; data data; sort sort; list list; configLoader cfload; configWriter cfwrite; FileDataBase fdb;
	utils (String[] args) { //инициализация заданных классов
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("mem")) {memory m = new memory(); mem = m;};
			if (args[i].equals("count")) {count c = new count(); count = c;};
			if (args[i].equals("data")) {data d = new data(); data = d;};
			if (args[i].equals("sort")) {sort s = new sort(); sort = s;};
			if (args[i].equals("list")) {list l = new list(); list = l;};
			if (args[i].equals("configLoader")) {configLoader c = new configLoader(); cfload = c;}
			if (args[i].equals("configWriter")) {configWriter w = new configWriter(); cfwrite = w;}
			if (args[i].equals("FileDataBase")) {FileDataBase db = new FileDataBase(); fdb = db;}
		}
	}
	utils (String arg) { //инициализация заданного класса
			if (arg.equals("mem")) {memory m = new memory(); mem = m;};
			if (arg.equals("count")) {count c = new count(); count = c;};
			if (arg.equals("data")) {data d = new data(); data = d;};
			if (arg.equals("sort")) {sort s = new sort(); sort = s;};
			if (arg.equals("list")) {list l = new list(); list = l;};
			if (arg.equals("configLoader")) {configLoader c = new configLoader(); cfload = c;}
			if (arg.equals("configWriter")) {configWriter w = new configWriter(); cfwrite = w;}
			if (arg.equals("FileDataBase")) {FileDataBase db = new FileDataBase(); fdb = db;}
	}
	
	//--------------------- методы
	public String remChars(String s, int p1, int p2) { //метод для вырезания символов из строк
		   String pp1, pp2, result;
		   if (p1 > -1 && p2 > p1 && p2 <= s.length()) {
			   	int pr = p2 - p1; pr = p1 + pr;
		   		if (p1 != 0) pp1 = s.substring(0, p1); else pp1 = s;
		   		if(p2 == s.length()) {if (p2 != pr) pp2 = s.substring(pr, p2); else pp2 = s;} else pp2 = s.substring(p2, s.length());
		   		if (pp1.equals(s)) result = pp2; else if (pp2.equals(s)) result = pp1; else result = pp1 + pp2;
		   	} else result = s;
		   return result;
	}
	
	//проверка наличия строки в массиве строк
	public boolean chW(String w, String[] ws) {
		boolean result = false;
		if (ws == null | ws.length == 0) return false; else {
		for (int i = 0; i < ws.length; i++) {
			if (w.equals(ws[i])) {result = true; break;} else result = false;
		}}
		return result;
	}
		
	public String cleform(String p) { //чистка форматирования
		while (p.lastIndexOf("§") > -1) {
			if ((p.lastIndexOf(" ") + 2) >= p.length()) break;
			p = remChars(p, p.lastIndexOf("§"), p.lastIndexOf("§") + 2);
		}
		return p;
	}
	
	public String cleChar(String p, String s) { //чистка от символа (+1 следующий символ)
		while (p.lastIndexOf(s) > -1) {
			if ((p.lastIndexOf(" ") + 2) >= p.length()) break;
			p = remChars(p, p.lastIndexOf(s), p.lastIndexOf(s) + 2);
		}
		return p;
	}
	
	public String trim(String s) { //чистка от пробелов
		return s.trim().replaceAll(" ", "");
	}
	
	public int getTime(int n, String s) { //определить кол-во секунд по обозначению
		int result = -1;
		if (s.equals("s") | s.equals("sec") | s.equals("second")) result = n; else
			if (s.equals("m") | s.equals("min") | s.equals("minute") | s.equals("minutes")) result = n*60; else
				if (s.equals("h") | s.equals("hour") | s.equals("hours")) result = n*60*60; else
					if (s.equals("d") | s.equals("day") | s.equals("days")) result = n*60*60*24; else
						if (s.equals("w") | s.equals("week") | s.equals("weeks")) result = n*60*60*24*7; else
							if (s.equals("mo") | s.equals("month") | s.equals("months")) result = n*60*60*24*30; else
								if (s.equals("y") | s.equals("year") | s.equals("years")) result = n*60*60*24*365; else
									result = n;
		return result;
	}
	
	String getPatch() { //получение директории запуска (сервера)
		String patch = System.getProperty("java.class.path");
		patch = remChars(patch, patch.lastIndexOf("/"), patch.length());
		return patch;
	}
	
	//извлечене конфига из плагина где conf - путь до конфига внутри плагина, file - путь до сохраняемого конфига
	void saveConfig(String conf, String file) {
		File fich = new File(file);
		if (!fich.isFile()) {
			File foch = new File(remChars(file, file.lastIndexOf("/"), file.length()));
			if (!foch.isDirectory()) foch.mkdirs();
			try {
				FileOutputStream fos = new FileOutputStream(fich);
				InputStream stream = AllFixer.class.getResourceAsStream(conf);
				byte[] buff = new byte[65536];
				int n;
				while((n = stream.read(buff)) > 0){
					fos.write(buff, 0, n);
					fos.flush();
				}
				fos.close();
				buff = null;
			} catch(Exception e) {
				log.info("saveConfig " + conf + " to " + file + " Exception:");
				e.printStackTrace();
			}
		}
	}
	
	public String[] mlcase(String[] s) { //все строки массива в нижний регистр
		for (int i = 0; i < s.length; i++) {
			if (s[i] != null) s[i] = s[i].toLowerCase();
		}
		return s;
	}
	
	public boolean isOnline(String p) { //проверить, в онлайне ли игрок
		boolean result = false;
		Player[] list = new Player[Bukkit.getServer().getOnlinePlayers().size()];
		int psize = Bukkit.getServer().getOnlinePlayers().size();
		Iterator<? extends Player> iter =  Bukkit.getServer().getOnlinePlayers().iterator();
		for (int i = 0; i < psize; i++) {
			list[i] = iter.next();
		}
		String names[] = new String[list.length];
		for (int i = 0; i < list.length; i++) {
			names[i] = cleform(list[i].getName()).toLowerCase().trim();
		}
		result = chW(p.toLowerCase().trim(), names);
		return result;
	}
	
	//--------------------- классы
	private class logger { //отправка сообщений в консоль
		private final Logger log = Logger.getLogger("Minecraft");
		boolean send = true;
		public void info(String text) {
			if (send) log.warning("[AllFixer] " + text);
		}
	}
	private logger log = new logger();
	void onLog() {log.send = true;} //включить вывод отладочной информации
	void offLog() {log.send = false;} //выключить вывод отладочной информации
	
	class memory { //память (нужно для некоторых функций)
		memory () {setSize(1);};
		memory (int val) {setSize(val);}
		private int size; private int step = 0; private int[] values;
		void setSize(int s) {values = new int[s + 1]; size = s + 1;}; //установка размера памяти
		void sendValue(int val) {if (step < size) {values[step] = val; step++;} else {step = 0; values[step] = val;};}; //сохранение значения
		void reset() {for (int i = 0; i < size; i++) values[i] = 0; step = 0;}; //очищение памяти
		int colVal(int val) {int v = 0; for (int i = 0; i < size; i++) {if (values[i] == val) v++;}; return v;}; //узнать кол-во занятых значением ячеек
		boolean isVal(int val) {int v = 0; for (int i = 0; i < size; i++) {if (values[i] == val) v++;}; if (v > 0) return true; else return false;}; //проверить наличие значения в памяти
		void delVal(int val) { //удаление значения
			for (int i = 0; i < values.length; i++) {
				if (val == values[i]) {
					values[i] = 0;
		}}}
		memory getMemory() { //получение класса
			memory mem = new memory();
			return mem;
		}
	}
	
	class count { //простой счетчик
		private int c = 0;
		void next() {if (c < 2147483647) this.c++;}; //следующее значение
		void reset() {this.c = 0;}; //сброс
		int get() {return c;}; //получение значения
		count getCount() { //получение класса
			count co = new count();
			return co;
		}
	}
	
	class data { //БД на двухмерном массиве
		private count co = new count();
		
		private String[][] data; private String[] player; private count[] co2;
		
		void setSize(int a, int b) {
			data = new String[a][b];
			player = new String[a];
			co2 = new count[a];
		}
		
		void sendData(String p, String d) { //установка значения
			if (p != null && d != null) {
			int slot = -1; int slot2 = -1;
			
			for (int i = 0; i < player.length; i++) { //проверка, есть ли уже такое значение в массиве
				if (player[i] != null) {if (player[i].equals(p)) {slot = i; break;}};
			}
			
			if (slot == -1) { //если значение не нашлось - используем счетчик
				if (co.get() < data.length) {
					slot = co.get();
					co.next();
				} else {
					co.reset();
					slot = co.get();
					co.next();
				}
			}
			
			for (int i = 0; i < data[slot].length; i++) { //поиск пустого слота в массиве сообщений игрока
				if (data[slot][i] == null) {slot2 = i; break;};
			}
			
			if (slot2 == -1) { //если массив заполнен
				if (co2[slot].get() < data[slot].length) {
					slot2 = co2[slot].get();
					co2[slot].next();
				} else {
					co2[slot].reset();
					slot2 = co2[slot].get();
					co2[slot].next();
				}
			}
			
			//запоминание значений
			this.data[slot][slot2] = d;
			this.player[slot] = p;
		}}
		
		String getData(String p, int s) { //получение значения
			String result = null;
			for (int i = 0; i < data.length; i++) {
				if (p.equals(player[i])) {result = data[i][s]; break;};
			}
			return result;
		}
		
		String[] getAllData(String p) { //получение массива сообщений на игрока
			if (p == null) return null;
			String result[] = new String[getLenInPlay(p)];
			int n = -1;
			for (int i = 0; i < data.length; i++) { //узнаем слот
				if (p.equals(player[i])) {n = i; break;};
			}
			if (n != -1) { //заполняем массив для отправки
				int s = 0;
				for (int i = 0; i < data.length; i++) {
					if (data[n][i] != null && s < result.length) {result[s] = data[n][i]; s++;};
				}
			} else result = null;
			return result;
		}
		
		String[] getPlayers() { //получение списка игроков массивом
			String play[] = new String[player.length]; int sl = 0;
			for (int i = 0; i < player.length; i++) {
				if (player[i] != null) {play[sl] = player[i]; sl++;};
			}
			String pl[] = new String[sl];
			for (int i = 0; i < sl; i++) {
				pl[i] = play[i];
			}
			return pl;
		}
		
		String getPlayersString() { //получение списка игроков строкой
			String list = null;
			String[] play = getPlayers();
			for (int i = 0; i < play.length; i++) {
				if (list == null) list = play[i]; else list += ", " + play[i];
			}
			return list;
		}
		
		int getRealLength() { //получение количества игроков в БД
			int result = 0;
			for (int i = 0; i < player.length; i++) {
				if (player[i] != null) result++;
			}
			return result;
		}
		
		int getLenInPlay(String p) { //получение кол-ва сообщений на игрока
			if (p == null) return 0;
			int result = 0;
			for (int i = 0; i < player.length; i++) {
				if (player[i] != null) {if (player[i].equals(p)) {
					for (int i2 = 0; i2 < data[i].length; i2++) {
						if (data[i][i2] != null) result++;
					}
					break;
					}
				}
			}
			return result;
		}
		
		void clearAll() { //очистка всей БД
			for (int i = 0; i < data.length; i++) {
				for (int i2 = 0; i2 < data.length; i2++) {
					data[i][i2] = null;
				}
				player[i] = null;
			}
		}
		
		boolean clearPlayer(String play) { //очистка записей на игрока
			boolean result = false;
			if (play != null && player != null && data != null) {
			for (int i = 0; i < player.length; i++) {
				if (player[i] != null && player[i].equals(play)) {
					for (int i2 = 0; i2 < data[i].length; i2++) {
						this.data[i][i2] = null;
					}
					this.player[i] = null;
					result = true;
					break;
				} else result = false;
			}} else result = false;
			return result;
		}
		
		boolean clearPlayerMess(String play, String mess) { //удаление конкретной записи на игрока
			boolean result = false;
			if (play != null && mess != null && player != null && data != null) {
				for (int i = 0; i < player.length; i++) {
					if (player[i] != null && player[i].equals(play)) {
						for (int i2 = 0; i2 < data[i].length; i2++) {
							if (data[i][i2] != null && data[i][i2].equals(mess)) {
								this.data[i][i2] = null;
								result = true;
								break;
			}}}}}
			return result;
		}
		
		data getData() { //получение класса
			data dat = new data();
			return dat;
		}
	}
	
	class sort { //разбивка на страницы для вывода сообщений
		private String[] data; private String[][] exit; 
		
		void sendData(String[] d) { //получение массива строк
			if (d != null) {
				this.data = new String[d.length];
				this.data = d;
			}
		}
		
		void go() { //разбивка на страницы с помощью двухмерно массива
			if (data != null && data.length > 0) {
				exit = new String[(data.length / 10) + 1][10];
				int n = 0;
				for (int i = 0; i < exit.length; i++) {
					for (int i2 = 0; i2 < 10; i2++) {
						if (n >= data.length) break;
						exit[i][i2] = data[n];
						n++;
					}
		}}}
		
		int getNumStr() { //получение кол-ва страниц
			int result = 0;
			if (exit != null && exit.length > 0) {
				result = exit.length;
			}
			return result;
		}
		
		String[] getStr(int n) { //получение страницы массивом
			String[] result = null;
			if (exit != null & n > -1 & n < exit.length && exit.length > 0 & exit[n] != null) {
				result = new String[exit[n].length];
				result = exit[n];
			}
			return result;
		}
		
		String[][] getAll() { //получение всего двухмерного массива строк
			if (exit != null && exit.length > 0) {
				return exit;
			} else return null;
		}
		
		sort getSort() { //получение всего класса
			sort s = new sort();
			return s;
		}
	}
	
	class list { //БД на одномерном массиве
		private String[] data;
		private count co = new count();
		
		public void setSize(int s) { //инициализация массива
			this.data = new String[s];
		}
		
		public void send(String d) { //сохранение сообщения в массив
			if (d != null) {
				int slot = co.get();
				if (slot < data.length) { //узнаем в какой слот писать значение
					co.next();
				} else {
					co.reset();
					slot = co.get();
					co.next();
				}
				this.data[slot] = d;
		}}
		
		public int getCol() { //получение количества записей
			int n = 0;
			for (int i = 0; i < data.length; i++) {
				if (data[i] != null) n++;
			}
			return n;
		}
		
		public String[] getData() { //получение массива записей
			String result[] = new String[getCol()];
			int n = 0;
			for (int i = 0; i < data.length; i++) {
				if (data[i] != null && n < result.length) {result[n] = data[i]; n++;};
			}
			return result;
		}
		
		public String getDataString() { //получение записей строкой
			String mas[] = new String[getData().length];
			mas = getData();
			String result = null;
			for (int i = 0; i < mas.length; i++) {
				if (result == null) result = mas[i]; else result += ", " + mas[i];
			}
			return result;
		}
		
		public void reset() { //очистка массива
			for (int i = 0; i < data.length; i++) data[i] = null;
			co.reset();
		}
		
		public void del(String s) { //удаление значения
			for (int i = 0; i < data.length; i ++) {
				if (s.equals(data[i])) data[i] = null;
			}
		}
	}
	
	class configLoader { //чтение конфига из файла и получение значений
		private boolean get = false;
		private String[] file;
		
		class result { //класс для передачи результата прогрузки файла
			String[] list;
			boolean io = false;
			boolean fnf = false;
			String IO;
			String FNF;
			boolean load = false;
			boolean nul = false;
			result getRes() {result res = new result(); return res;}
		}
		
		void load(String f) { //загрузка конфина
			result result = new result();
			result = getList(f);
			if (result.load == true) {
				this.file = new String[result.list.length];
				this.file = result.list;
				this.get = true;
			} else {
				if (result.io == true) {
					log.info("IOException при загрузке конфига " + f + ":");
					log.info(result.IO);
				}
				if (result.fnf == true) {
					log.info("FileNotFoundException при загрузке конфига " + f + ":");
					log.info(result.FNF);
				}
				if (result.nul == true) {
					log.info("нулевая длинна конфига " + f);
				}
				
			}
		}
		
		void fakeload(String[] value) { //фейковая загрузка (установка значения из массива)
			this.file = value;
			this.get = true;
		}
		
		result getList(String f) { //получаем текстовый файл массивом, очищенный от комментов
			String[] result;
			result res = new result();
			try {
				BufferedReader text = new BufferedReader(new FileReader(f));
				try { //узнаем кол-во строк, инициализируем и заполняем массив
					int l = 0;
					ArrayList<String> list = new ArrayList<String>();
					String line = null;
					do {
						line = text.readLine();
						if (line != null) {
							if (line.trim().indexOf("#") != 0 && trim(line).length() > 1) { //если строка - комментарий или пустая, не добавляем ее
								list.add(line);
								l++;
							}
						}
					} while (line != null && l < 2147483647);
					text.close();
					if (l > 0) { //проверка на нулевую длинну
						result = new String[l];
						for (int i = 0; i < l; i++) result[i] = list.get(i); //заполнение массива
					} else {result = null; res.nul = true;}
				} catch(IOException e) {result = null; res.io = true; res.IO = e.getMessage();}
			} catch(FileNotFoundException e) {result = null; res.fnf = true; res.FNF = e.getMessage();}
			if (result != null) { //сохранение результата
				result = clear(result);
				res.list = new String[result.length];
				res.list = result;
				res.load = true;
			}
			return res;
		}
		
		String[] clear(String[] s) { //чистка от #комментов
			if (s == null) {log.info("configLoader clear: String is null"); return null;}
			for(int i = 0; i < s.length; i++) {
				if (s[i] != null) {
				while (s[i].indexOf("#") > -1) {
					String ch = s[i];
					int check = ch.indexOf('"'); //исключаем данные из параметра
					int check2 = ch.lastIndexOf('"');
					if (check > -1 & check2 > -1 & check2 > check) {
						ch = remChars(s[i], check, check2);
					}
					if (ch.indexOf("#") > -1) { //если коммент все же есть, удаляем его
						ch = s[i];
						s[i] = remChars(s[i], s[i].indexOf("#"), s[i].length());
					}
					if (ch.equals(s[i])) s[i] = " "; //на случай ошибок обрезки, в основном когда коммент на всю строку
				}}
			}
			return s;
		}
		
		private String getString(int index) { //получение переменной типа String по индексу
			String result = null;
			if (index < 0) {log.info("configLoader getString: failed, index < 0"); return result;}
			if (get == true && this.file != null) { //поиск и получение переменной из массива
				for (int i = 0; i < file.length; i++) {
					if (file[i] != null) {
						result = file[index];
				}}
			} else if (get == false) log.info("configLoader getString(index): " + index + "(index) file not loaded");
			else if (file == null) log.info("configLoader getString(index): " + index + "(index) array file = null");
			if (result != null && result.indexOf(":") > -1) { //обрезка до двоеточия
				result = remChars(result, 0, result.indexOf(":")+1).trim();
				//обрезка от скобки до скобки, если они есть
				int ch = result.indexOf('"');
				int ch2 = result.lastIndexOf('"');
				if (ch > -1 & ch2 > -1 & ch2 > ch) {
					result = remChars(result, 0, ch+1);
					result = remChars(result, ch2-1, result.length());
				}
			} else if (result != null && result.indexOf(":") == -1) {log.info("configLoader getString(index): " + index + "(index) not ':', ride error"); result = null;}
			if (result == null) log.info("configLoader getString(index): " + index + "(index) = null");
			return result;
		}
		
		String getString(String name) { //получение переменной типа String по названию
			String result = null;
			if (name == null) {log.info("configLoader getString: null name"); return result;}
			if (get == true && this.file != null) {
				result = getString(getIndexNoSection(name));
			} else log.info("configLoader getString(name): " + name + " error (file not load or null array");
			if (result == null) log.info("configLoader getString(name): " + name + " error, var not found");
			return result;
		}
		
		private int getInt(int index) { //получение переменной типа int по индексу
			String str = getString(index);
			String name = getName(index);
			if (str != null) str = trim(str).toLowerCase(); else log.info("configLoader getInt: " + name + " str = null");
			int num = 0;
			String error = null;
			try {
				if (str != null) {
					num = Integer.parseInt(str);
				} else {
					error = "configLoader getInt: " + name + " null String";
				}
			} catch(NumberFormatException e) {error = "configLoader getInt: " + name + " NumberFormatException: " + e.getMessage();}
			if (error != null) log.info(error);
			return num;
		}
		
		int getInt(String name) { //получение переменной типа int по названию
			if (name == null) {log.info("configLoader getInt: null name"); return -1;}
			return getInt(getIndexNoSection(name));
		}
		
		private long getLong(int index) { //получение переменной типа long по индексу
			String name = getName(index);
			String str = getString(index);
			if (str != null) str = trim(str).toLowerCase(); else log.info("configLoader getLong: " + name + " str = null");
			long num = 0;
			String error = null;
			try {
				if (str != null) {
					num = Long.parseLong(str);
				} else error = "configLoader getLong: " + name + " null String";
			} catch(NumberFormatException e) {error = "configLoader getLong: " + name + " NumberFormatException: " + e.getMessage();}
			if (error != null) log.info(error);
			return num;
		}
		
		long getLong(String name) { //получение переменной типа long по названию
			if (name == null) {log.info("configLoader getLong: null name"); return -1;}
			return getLong(getIndexNoSection(name));
		}
		
		private double getDouble(int index) { //получение переменной типа double по индексу
			String str = getString(index);
			String name = getName(index);
			if (str != null) str = trim(str).toLowerCase(); else log.info("configLoader getDouble: " + name + " str = null");
			double num = 0;
			String error = null;
			try {
				if (str != null) {
					num = Double.parseDouble(str);
				} else error = "configLoader getDouble: " + name + " null String";
			} catch(NumberFormatException e) {error = "configLoader getDouble: " + name + " NumberFormatException: " + e.getMessage();}
			if (error != null) log.info(error);
			return num;
		}
		
		double getDouble(String name) { //получение переменной типа double по названию
			if (name == null) {log.info("configLoader getDouble: null name"); return -1;}
			return getDouble(getIndexNoSection(name));
		}
		
		private boolean getBoolean(int index) { //получение переменной типа boolean по индексу
			String str = getString(index);
			String name = getName(index);
			if (str != null) str = trim(str).toLowerCase(); else log.info("configLoader getBoolean: " + name + " str = null");
			boolean res = false;
			if (str != null && str.equals("true") | str.equals("false")) {
				res = Boolean.parseBoolean(str);
			} else if(str == null) log.info("configLoader getBoolean: " + name  + " null String"); else
				log.info("configLoader getBoolean: " + name + " var not boolean");
			return res;
		}
		
		boolean getBoolean(String name) { //получение переменной типа boolean по названию
			if (name == null) {log.info("configLoader getBoolean: null name"); return false;}
			return getBoolean(getIndexNoSection(name));
		}
		
		String[] getAll() { //получение всего конфига массивом строк
			if (get == true && file != null) return file; else {
				log.info("configLoader getAll: failed, returning null");
				return null;
			}
		}
		
		private String[] getStringArray(int index) { //получение переменной типа массив строк (по индексу)
			String[] result = null;
			if (index < 0) {log.info("configLoader getStringArray: failed, index < 0"); return result;}
			int pos = -1;
			if (this.get == true && file != null) {
				if (file[index] != null) pos = index;
			} else log.info("configLoader getStringArray(index): get " + index + " failed (config not loaded or file = null)");
			if (pos != -1) { //если переменная найдена, то начинаем проверку и последующее извелечение данных
				isarray is = new isarray();
				is = isArray(file[pos], pos); //проверка, является ли переменная массивом и если да, то каким именно
				if(is.isArray) {
					if(is.isSkobka) { //получение массива, заключенного в квадратные скобки
						String arr = file[pos];
						boolean empty; //проверка пустой ли массив (просто [])
						if ((arr.lastIndexOf("]") - (arr.indexOf("["))) > 2) empty = false; else empty = true;
						arr = remChars(arr, 0, arr.indexOf("[")+1);
						arr = remChars(arr, arr.lastIndexOf("]"), arr.length());
						if (trim(arr).length() > 2 && empty == false) empty = false; else empty = true; //еще проверка
						if (empty == false) {
						String[] result2 = null;
						result2 = arr.split(",");
						int resleng = 0;
						for(int i = 0; i < result2.length; i++) {
							if (result2[i] != null) resleng++;
						}
						result = new String[resleng];
						for(int i = 0; i < resleng; i++) {
							if (result2[i] != null) {
								result[i] = result2[i].trim();
								//обрезка от скобки до скобки, если они есть
								int ch = result[i].indexOf('"');
								int ch2 = result[i].lastIndexOf('"');
								if (ch > -1 & ch2 > -1 & ch2 > ch) {
									result[i] = remChars(result[i], 0, ch+1);
									result[i] = remChars(result[i], ch2-1, result[i].length());
								}
							}
						}
						} else result = new String[0]; //если массив пустой (просто [])
					} else { //получение массива, перечисленного через тире
						int leng = 0;
						int pp = pos+1;
						while(isArray(file[pp])) {leng++; pp++; if (pp >= file.length) break;}
						result = new String[leng];
						pp = pos+1;
						for(int i = 0; i < leng; i++) {
							result[i] = remChars(file[pp], 0, file[pp].indexOf("-")+1).trim();
							//обрезка от скобки до скобки, если они есть
							int ch = result[i].indexOf('"');
							int ch2 = result[i].lastIndexOf('"');
							if (ch > -1 & ch2 > -1 & ch2 > ch) {
								result[i] = remChars(result[i], 0, ch+1);
								result[i] = remChars(result[i], ch2-1, result[i].length());
							}
							pp++;
						}
					}
				} else log.info("configLoader getStringArray(index): var " + index + " not array");
				if (is.isCheck == false) log.info("configLoader getStringArray(index): " + index + " failed check is a array");
			} else log.info("configLoader getStringArray(index): var " + index + " not found");
			return result;
		}
		
		String[] getStringArray(String name) { //получение переменной типа массив строк (по названию)
			String[] result = null;
			if (name == null) {log.info("configLoader getStringArray(name): null name"); return result;}
			if (this.get == true && file != null) {
				result = getStringArray(getIndexNoSection(name));
			} else log.info("configLoader getStringArray(name): get " + name + " failed (config not loaded or file = null)");
			if (result == null) log.info("configLoader getStringArray(name): " + name + " error, var not found");
			return result;
		}
		
		private class isarray { //класс для возвращение результата проверки переменной на массив
			boolean isArray = false; //массив ли эта переменная
			boolean isSkobka = false; //данные в квадратных скобках или через тире
			boolean isCheck = false; //удалась ли проверка
		}
		
		private isarray isArray(String s, int p) { //проверка переменной на то, является ли она массивом
			boolean result = false;
			isarray res = new isarray();
			if (s == null) return res;
			int ps1 = s.indexOf("[");
			int ps2 = s.lastIndexOf("]");
			if (ps1 > -1 & ps2 > -1 & ps2 > ps1) { //проверка, заключены ли данные массива в квадратные скобки
				result = true;
				res.isSkobka = true;
				res.isCheck = true;
			}
			if (result == false && get == true && file != null) { //если нет, то проверка, не перечислены ли они через тире
				if ((p+1) < file.length) {
					res.isCheck = true;
					result = isArray(file[p+1]);
			}} else if (result != false && get == false | file == null) log.info("configLoader isArray: " + s + " failed check, not loaded config");
			res.isArray = result;
			return res;
		}
		
		private boolean isArray(String s) { //проверка, является ли строка компонентом массива (т.е. начинается с тире)
			boolean result = false;
			if (s == null) return result;
			String check = s;
			boolean check2 = false; //является ли тире первым символом в строке
			int tir = check.indexOf("-");
			if (tir > -1) { //проверка, является ли тире первым символом в строке (в таком случае это - ячейка массива)
				String check3 = trim(check).substring(0, 1);
				if (check3.equals("-")) check2 = true; else check2 = false;
			}
			result = check2;
			return result;
		}
		
		private int[] getIntArray(int index) { //получение массива типа int по индексу
			int[] result = null;
			String error = null;
			String[] text = getStringArray(index);
			String name = getName(index);
			if (text != null) {
			result = new int[text.length];
			for(int i = 0; i < text.length; i++) {
				try {
					result[i] = Integer.parseInt(text[i]);
				} catch(NumberFormatException e) {error = "configLoader getIntArray: " + name + " index of " + i + " NumberFormatException: " + e.getMessage();}
			}} else log.info("configLoader getIntArray: " + name + " null text (StringArray)");
			if (error != null) log.info(error);
			return result;
		}
		
		int[] getIntArray(String name) { //получение массива типа int по названию
			if (name == null) return null;
			return getIntArray(getIndexNoSection(name));
		}
		
		private long[] getLongArray(int index) { //получение массива типа long по индексу
			long[] result = null;
			String error = null;
			String[] text = getStringArray(index);
			String name = getName(index);
			if (text != null) {
			result = new long[text.length];
			for(int i = 0; i < text.length; i++) {
				try {
					result[i] = Long.parseLong(text[i]);
				} catch(NumberFormatException e) {error = "configLoader getLongArray: " + name + " index of " + i + " NumberFormatException: " + e.getMessage();}
			}} else log.info("configLoader getLongArray: " + name + " null text (StringArray)");
			if (error != null) log.info(error);
			return result;
		}
		
		long[] getLongArray(String name) { //получение массива типа long по названию
			if (name == null) return null;
			return getLongArray(getIndexNoSection(name));
		}
		
		private double[] getDoubleArray(int index) { //получение массива типа double по индексу
			double[] result = null;
			String error = null;
			String[] text = getStringArray(index);
			String name = getName(index);
			if (text != null) {
			result = new double[text.length];
			for(int i = 0; i < text.length; i++) {
				try {
					result[i] = Double.parseDouble(text[i]);
				} catch(NumberFormatException e) {error = "configLoader getLongArray: " + name + " index of " + i + " NumberFormatException: " + e.getMessage();}
			}} else log.info("configLoader getDoubleArray: " + name + " null text (StringArray)");
			if (error != null) log.info(error);
			return result;
		}
		
		double[] getDoubleArray(String name) { //получение массива типа double по названию
			if (name == null) return null;
			return getDoubleArray(getIndexNoSection(name));
		}
		
		private boolean[] getBooleanArray(int index) { //получение массива типа boolean по индексу
			boolean[] result = null;
			String error = null;
			String[] text = getStringArray(index);
			String name = getName(index);
			if (text != null) {
			result = new boolean[text.length];
			for(int i = 0; i < text.length; i++) {
				if (text[i].equals("true") | text[i].equals("false")) {
					result[i] = Boolean.parseBoolean(text[i]);
				} else error = "configLoader getBooleanArray: " + name + " error, var index " + i + " in array not boolean";
			}} else log.info("configLoader getBooleanArray: " + name + " null text (StringArray)");
			if (error != null) log.info(error);
			return result;
		}
		
		boolean[] getBooleanArray(String name) { //получение массива типа boolean по названию
			if (name == null) return null;
			return getBooleanArray(getIndexNoSection(name));
		}
		
		private boolean isSet(int index) { //проверка, прописана ли переменная (по индексу)
			boolean result = false;
			if (index < 0) {log.info("configLoader isSet: failed, index < 0"); return result;}
			if (index >= file.length) {log.info("configLoader isSet: failed, index > file.length"); return result;}
			if (get == true && this.file != null) {
				result = isParam(index); //является ли строка параметром
				if (index+1 < file.length) {if (!result && isArray(file[index+1])) result = true;} //является ли она массивом
				if (!result && index+1 < file.length) {
					if (file[index+1].indexOf(":") > 1) result = true; //является ли она секцией (упрощенный вариант проверки)
				}
			} else log.info("configLoader isSet(index): failed check " + index + ", config not loaded");
			return result;
		}
		
		boolean isSet(String name) { //проверка, прописана ли переменная (по названию)
			boolean result = false;
			if (name == null) {log.info("configLoader isSet: null name"); return false;}
			if (get == true && this.file != null) {
				result = isSet(getIndexNoSection(name));
			} else log.info("configLoader isSet(name): failed check " + name + ", config not loaded");
			return result;
		}
		
		private boolean isSetArray(int index) { //проверка, прописан ли массив (по индексу)
			int pos = -1;
			boolean result = false;
			String name = getName(index);
			if (this.get == true && file != null) {
				pos = index;
				if (pos > -1) { //если переменная найдена
					isarray isr = isArray(file[pos], pos);
					result = isr.isArray;
					if (result && !isr.isSkobka) { //если массив через тире, то проверяем, есть ли хотя бы 1 элемент
						result = isArray(file[pos+1]);
						if (result) { //проверка, не пустой ли этот элемент
							if (trim(file[pos+1]).length() > 1) result = true; else result = false;
						}
					}
					if (result && isr.isSkobka) { //если массив через квадратные скобки, проверяем, есть ли там хотя бы 1 символ
						String arr = file[pos];
						arr = remChars(arr, 0, arr.indexOf("[")+1);
						arr = remChars(arr, arr.lastIndexOf("]"), arr.length());
						if (trim(arr).length() > 2) result = true; else result = false;
					}
				}
			} else log.info("configLoader isSet: failed check " + name + ", config not loaded");
			return result;
		}
		
		boolean isSetArray(String name) { //проверка, прописан ли массив (по названию)
			if (name == null) {log.info("configLoader isSetArray: null name"); return false;}
			return isSetArray(getIndexNoSection(name));
		}
		
		private int getProbels(int index) { //узнаем кол-во пробелов в начале строки
			int result = -1;
			if (index < 0) {log.info("configLoader getProbels: failed, index < 0"); return result;}
			if (get == true && this.file != null & this.file[index] != null) {
				String str = file[index];
				String name = remChars(str.trim(), str.indexOf(":"), str.length());
				result = str.indexOf(name); //где первый символ названия и есть кол-во пробелов до него
			} else log.info("configLoader getProbels: failed check " + index + ", config not loaded or file[i] == null");
			return result;
		}
		
		private boolean isParam(int index) { //является ли строка параметром
			boolean result = false;
			if (index < 0) {log.info("configLoader isParam: failed, index < 0"); return result;}
			if (get == true && this.file != null & this.file[index] != null) {
				String str = file[index].trim();
				if (str.indexOf(":") > 1 && !isArray(file[index])) { //проверка, есть ли что-то после двоеточия (элементы массивов не учитываем)
					String afterr[] = str.split(":");
					if (afterr.length > 1) {
						String after = afterr[1];
						if (after != null) {
							after = trim(after);
							if (after.length() > 0) result = true;
						}
					}
				}
			} else log.info("configLoader isParam: failed check " + index + ", config not loaded or file[i] == null");
			return result;
		}
		
		private String getName(int index) { //получение названия переменной по индексу
			String result = null;
			if (index < 0) {log.info("configLoader getName: failed, index < 0"); return result;}
			if (get == true && this.file != null & this.file[index] != null && this.file[index].indexOf(":") != -1) {
				result = file[index].trim();
				result = remChars(result, result.indexOf(":"), result.length());
			} else log.info("configLoader getName: failed check " + index + ", config not loaded or file[i] == null");
			return result;
		}
		
		private int getIndexSection (String name) { //получить индекс секции по названию
			int result = -1;
			if (name == null) {log.info("configLoader getIndexSection: null name"); return result;}
			if (this.get == true && file != null) {
				for (int i = 0; i < file.length; i++) {
					if (file[i] != null) {
						if (remChars(file[i], file[i].indexOf(":"), file[i].length()).trim().equals(name)) {
							if (isSection(i)) {
								result = i;
								break;
							}
						}
				}}
			} else log.info("configLoader getIndexSection: failed get " + name + " config not loaded or file == null");
			return result;
		}
		
		private int getIndexNoSection (String name) { //получить индекс переменной по названию (не секции)
			int result = -1;
			if (name == null) {log.info("configLoader getIndexNoSection: null name"); return result;}
			if (this.get == true && file != null) {
				for (int i = 0; i < file.length; i++) {
					if (file[i] != null) {
						if (remChars(file[i], file[i].indexOf(":"), file[i].length()).trim().equals(name)) {
							if (isSet(i) && !isSection(i)) {
								result = i;
								break;
							}
						}
				}}
			} else log.info("configLoader getIndexNoSection: failed get " + name + " config not loaded or file == null");
			return result;
		}
		
		private boolean isSection(int index) { //проверка, является ли переменная секцией (по индексу)
			boolean result = false;
			if (index < 0) {log.info("configLoader isSection: failed, index < 0"); return result;}
			if (this.get == true && file != null) {
				int posprob = getProbels(index), next = index+1;
				if (next < file.length) {
					int nextprob = getProbels(next);
					if (!isParam(index) & !isArray(file[next]) && nextprob > posprob) result = true;
					if (result) {
						boolean param = false; int i = next;
						do {
							int p = getProbels(i);
							if (p > posprob) {
								param = isSet(i);
							} else break;
							if (param) break;
							i++;
							if (i >= file.length) break;
						} while (!param);
						result = param; //если в секции не было параметров, значит это не секция
					}
				}
			} else log.info("configLoader isSection(index): failed check " + index + ", config not loaded or file[i] == null");
			return result;
		}
		
		boolean isSection(String name) { //проверка, является ли переменная секцией (по названию)
			boolean result = false;
			if (name == null) {log.info("configLoader isSection: null name"); return false;}
			if (this.get == true && file != null) {
				result = isSection(getIndexSection(name));
			} else log.info("configLoader isSection(name): failed check " + name + ", config not loaded or file[i] == null");
			return result;
		}
		
		String[] getSectionVars(String name) { //получение названий переменных секции
			String[] result = null;
			if (name == null) {log.info("configLoader getSectionVars: null name"); return result;}
			if (this.get == true && file != null) {
				if (isSection(name)) {
					int index = getIndexSection(name), prob = getProbels(index);
					ArrayList<String> list = new ArrayList<String>();
					for (int i = index+1; i < file.length; i++) { //подсчитываем кол-во переменных
						int p = getProbels(i); //вложенные секции не учитываем
						if (p > prob && isSet(i)) {
							list.add(getName(i));
						}
					}
					int vars = list.size();
					result = new String[vars];
					for (int i = 0; i < vars; i++) { //заполняем массив
						result[i] = list.get(i);
					}
				}
			} else log.info("configLoader isSection: failed check " + name + ", config not loaded or file[i] == null");
			return result;
		}
		
		int getSectionLength(String name) { //получение длинны секции (кол-ва переменных)
			int result = -1;
			if (name == null) {log.info("configLoader getSectionLength: null name"); return result;}
			if (this.get == true && file != null) {
				if (isSection(name)) {
					int index = getIndexSection(name);
					int p = getProbels(index);
					int p2 = 0, i = index+1;
					result = 0;
					do {
						p2 = getProbels(i);
						if (p2 > p && isSet(i)) result++;
						i++;
						if (i >= file.length) break;
					} while (p2 > p);
				}
			} else log.info("configLoader getSectionLength: failed get " + name + " config not loaded or file == null");
			return result;
		}
		
		int getSectionRealLength(String name) { //получение реальной длинны секции в конфиге (кол-во строк)
			int result = -1;
			if (name == null) {log.info("configLoader getSectionRealLength: null name"); return result;}
			if (this.get == true && file != null) {
				if (isSection(name)) {
					int index = getIndexSection(name);
					int p = getProbels(index);
					int p2 = 0, i = index+1;
					result = 0;
					do {
						p2 = getProbels(i);
						if (p2 > p) result++;
						i++;
						if (i >= file.length) break;
					} while (p2 > p);
				}
			} else log.info("configLoader getSectionRealLength: failed get " + name + " config not loaded or file == null");
			return result;
		}
		
		String[] getSectionNames() { //получение названий секций во всем конфиге
			String[] result = null;
			if (this.get == true && file != null) {
				ArrayList<String> list = new ArrayList<String>();
				for (int i = 0; i < file.length; i++) {
					if (file[i] != null) {
						if (isSection(i)) list.add(getName(i));
					}
				}
				int l = list.size();
				result = new String[l];
				for (int i = 0; i < l; i++) {
					result[i] = list.get(i);
				}
			} else log.info("configLoader getSectionNames: failed get sections names, config not loaded or file == null");
			return result;
		}
		
		String[] getSectionNames(String name) { //получение названий секций в секции
			String[] result = null;
			if (name == null) {log.info("configLoader getSectionNames: null name"); return result;}
			if (this.get == true && file != null) {
				int index = getIndexSection(name);
				int p = getProbels(index);
				ArrayList<String> list = new ArrayList<String>();
				for (int i = index+1; i < file.length; i++) {
					int p2 = getProbels(i);
					if (p2 > p) {
						if (isSection(i)) list.add(getName(i));
					} else break;
				}
				int l = list.size();
				result = new String[l];
				for (int i = 0; i < l; i++) {
					result[i] = list.get(i);
				}
			} else log.info("configLoader getSectionNames: failed get " + name + " config not loaded or file == null");
			return result;
		}
		
		private int getIndexInSection(String section, String name) { //получение индекса переменной в секции
			int result = -1;
			if (name == null | section == null) {log.info("configLoader getIndexInSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				if (isSection(section)) {
					int index = getIndexSection(section);
					int p = getProbels(index);
					int p2 = 0, i = index+1;
					do {
						p2 = getProbels(i);
						if (p2 > p && isSet(i)) {
							if (getName(i).equals(name)) {
								result = i;
								break;
							}
						}
						i++;
						if (i >= file.length) break;
					} while (p2 > p);
				}
			} else log.info("configLoader getIndexInSection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		boolean isSetInSection(String section, String name) { //проверка, установлена ли переменная в сеции
			boolean result = false;
			if (name == null | section == null) {log.info("configLoader isSetInSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = isSet(getIndexInSection(section, name));
			} else log.info("configLoader isSetInSection: failed check " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		boolean isSetArrayInSection(String section, String name) { //проверка, установлен ли массив в секции
			boolean result = false;
			if (name == null | section == null) {log.info("configLoader isSetArrayInSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = isSetArray(getIndexInSection(section, name));
			} else log.info("configLoader isSetArrayInSection: failed check " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		String getStringSection(String section, String name) { //получение переменной типа String из секции
			String result = null;
			if (name == null | section == null) {log.info("configLoader getStringSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getString(getIndexInSection(section, name));
			} else log.info("configLoader getStringSection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		int getIntSection(String section, String name) { //получение переменной типа int из секции
			int result = -1;
			if (name == null | section == null) {log.info("configLoader getIntSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getInt(getIndexInSection(section, name));
			} else log.info("configLoader getIntSection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		long getLongSection(String section, String name) { //получение переменной типа long из секции
			long result = -1;
			if (name == null | section == null) {log.info("configLoader getLongSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getLong(getIndexInSection(section, name));
			} else log.info("configLoader getLongSection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		double getDoubleSection(String section, String name) { //получение переменной типа double из секции
			double result = -1;
			if (name == null | section == null) {log.info("configLoader getDoubleSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getDouble(getIndexInSection(section, name));
			} else log.info("configLoader getDoubleSection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		boolean getBooleanSection(String section, String name) { //получение переменной типа boolean из секции
			boolean result = false;
			if (name == null | section == null) {log.info("configLoader getBooleanSection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getBoolean(getIndexInSection(section, name));
			} else log.info("configLoader getBooleanSection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		String[] getStringArraySection(String section, String name) { //получение переменной типа массив String из секции
			String[] result = null;
			if (name == null | section == null) {log.info("configLoader getStringArraySection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getStringArray(getIndexInSection(section, name));
			} else log.info("configLoader getStringArraySection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		int[] getIntArraySection(String section, String name) { //получение переменной типа массив int из секции
			int[] result = null;
			if (name == null | section == null) {log.info("configLoader getIntArraySection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getIntArray(getIndexInSection(section, name));
			} else log.info("configLoader getIntArraySection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		long[] getLongArraySection(String section, String name) { //получение переменной типа массив long из секции
			long[] result = null;
			if (name == null | section == null) {log.info("configLoader getLongArraySection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getLongArray(getIndexInSection(section, name));
			} else log.info("configLoader getLongArraySection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		double[] getDoubleArraySection(String section, String name) { //получение переменной типа массив double из секции
			double[] result = null;
			if (name == null | section == null) {log.info("configLoader getDoubleArraySection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getDoubleArray(getIndexInSection(section, name));
			} else log.info("configLoader getDoubleArraySection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		boolean[] getBooleanArraySection(String section, String name) { //получение переменной типа массив boolean из секции
			boolean[] result = null;
			if (name == null | section == null) {log.info("configLoader getBooleanArraySection: null name or null section"); return result;}
			if (this.get == true && file != null) {
				result = getBooleanArray(getIndexInSection(section, name));
			} else log.info("configLoader getBooleanArraySection: failed get " + name + " in " + section + " config not loaded or file == null");
			return result;
		}
		
		configLoader getLoader() {configLoader loader = new configLoader(); return loader;} //получение всего класса
		
		class methods { //класс со внутренними методами
			methods getMethods() {methods m = new methods(); return m;} //получить весь класс
			int RecIndexNoSection(String name) { //узнать индекс параметра по имени
				return getIndexNoSection(name);
			}
			String RecName(int index) { //узнать имя параметра по индексу
				return getName(index);
			}
			int RecIndexSection(String name) { //узнать индекс секции по названию
				return getIndexSection(name);
			}
			boolean IsSection(int index) { //проверить, является ли параметр секцией (по индексу)
				return isSection(index);
			}
			int RecIndexInSection(String section, String name) { //получить индекс параметра в секции
				return getIndexInSection(section, name);
			}
			boolean IsParam(int index) { //ялвяется ли строка параметром
				return isParam(index);
			}
			int RecProbels(int index) { //получить кол-во пробелов в начале строки
				return getProbels(index);
			}
			boolean IsSetArray(int index) { //проверка, прописан ли массив
				return isSetArray(index);
			}
			String RecString(int index) { //получить строку по индексу
				return getString(index);
			}
			String[] RecStringArray(int index) { //получить массив строк по индексу
				return getStringArray(index);
			}
			isarray getIsArrayResult() { //получить внутренний класс isarray
				isarray is = new isarray();
				return is;
			}
			isarray IsArray(String line, int index) { //проверить массив внутренним методом isArray
				return isArray(line, index);
			}
		}
		methods methods = new methods();
	}
	
	class configWriter { //запись и изменение конфига
		private String file[] = null;
		private boolean set = false, neew = true;
		private String patch = null;
		
		boolean setConfig(String path) { //установка конфига
			boolean result = false;
			File f = new File(path);
			result = f.isFile();
			if (!result) { //создаем конфиг, если его нет
				try {
					if (!f.createNewFile()) log.info("configWriter: не удалось создать " + path); else {
						set = true;
						patch = path;
						neew = true;
						this.file = null;
					}
				} catch (Exception e) {e.printStackTrace();}
			} else { //если есть, будем его перезаписывать
				set = true; neew = false;
				patch = path;
				configLoader loader = new configLoader();
				loader.load(path);
				this.file = loader.getAll();
			}
			return result;
		}
		
		private void writeFile() { //запись массива file
			if (set && file != null & patch != null) {
				try { //запись массива
					BufferedWriter write = new BufferedWriter(new FileWriter(patch));
					for (int i = 0; i < file.length; i++) {
						if (i != 0) write.newLine();
						write.write(file[i]);
					}
					write.close();
				} catch (Exception e) {e.printStackTrace();}
			} else log.info("configWriter: запись массива file не удалась, конфиг не установлен");
		}
		
		private void setOption(String name, String value, int ind) { //запись значения переменной
			if (set && patch != null) {
				if (neew) { //если файл новый, то просто пишем в него опцию
					this.file = new String[1];
					this.file[0] = name + ": " + value;
					writeFile();
					this.neew = false;
				} else if (file != null) { //если конфиг уже есть, заменяем значение и перезаписываем массив строк
					if (file.length > 0) {
						configLoader loader = new configLoader();
						loader.fakeload(file);
						int index = -1;
						if (ind == -1) index = loader.methods.RecIndexNoSection(name); else index = ind;
						if (index > -1) { //перезаписываем переменную, если она есть
							String rep = file[index].split(":")[0];
							rep += ": " + value; //подменяем значение
							this.file[index] = rep;
							writeFile();
						} else { //если нет, то добавляем ее в конфиг
							ArrayList<String> newfile = new ArrayList<String>();
							for (int i = 0; i < file.length; i++) newfile.add(file[i]);
							newfile.add(name + ": " + value);
							this.file = new String[newfile.size()];
							for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
							writeFile();
						}
					} else log.info("configWriter: запись значения " + name + " не удалась, переменная не найдена");
				} else log.info("configWriter: ошибка при записи параметра " + name + ", конфиг не установлен");
			} else log.info("configWriter: ошибка при записи параметра " + name + ", конфиг не установлен");
		}
		
		void setOption(String name, String value) { //для внешних обращений
			setOption(name, value, -1);
		}
		
		private void setArray(String name, String[] value, boolean skobka, int ind) { //запись значения массива
			if (set && patch != null) {
				if (neew) { //есди записываем в новый конфиг
					if (skobka) { //если значения в квадратных скобках
						String send = name + ": [ " + value[0];
						for (int i = 1; i < value.length; i++) { //парсим в строку
							send += ", " + value[i]; 
						}
						send += " ]";
						this.file = new String[1];
						this.file[0] = send;
						writeFile();
						this.neew = false;
					} else { //если через тире
						String send[] = new String[value.length + 1];
						send[0] = name + ":";
						for (int i = 0; i < value.length; i++) {
							send[i+1] = "- " + value[i];
						}
						this.file = send;
						writeFile();
						this.neew = false;
					}
				} else if (file.length > 0) { //если нужно заменить значение в старом конфиге
					configLoader loader = new configLoader();
					loader.fakeload(file);
					int index = ind;
					if (index == -1) index = loader.methods.RecIndexNoSection(name);
					if (skobka) { //если значение в квадратных скобках
						if (index > -1) { //заменяем значение в параметре
							String rep = name + ": [ " + value[0];
							if (file[index].split(":").length > 1) rep = file[index].split(":")[0] + ": [ " + value[0];
							for (int i = 1; i < value.length; i++) {
								rep += ", " + value[i]; 
							}
							rep += " ]";
							this.file[index] = rep;
							writeFile();
						} else { //записываем массив, если его нет в конфиге
							ArrayList<String> newfile = new ArrayList<String>();
							for (int i = 0; i < file.length; i++) newfile.add(file[i]);
							String rep = name + ": [ " + value[0];
							for (int i = 1; i < value.length; i++) {
								rep += ", " + value[i]; 
							}
							rep += " ]";
							newfile.add(rep);
							this.file = new String[newfile.size()];
							for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
							writeFile();
						}
					} else { //если массив через тире
						if (index > -1) {
							String oldfile[] = loader.methods.RecStringArray(index);
							if (oldfile.length == value.length) { //если длинна одинакова, заменяем данные
								int pos = index+1;
								for (int i = 0; i < oldfile.length; i++) {
									if (pos >= file.length) break;
									this.file[pos] = file[pos].split("-")[0] + "- " + value[i];
									pos++;
								}
								writeFile();
							} else { //если нет, расширяем массив file
								ArrayList<String> newfile = new ArrayList<String>();
								for (int i = 0; i <= index; i++) newfile.add(file[i]);
								String prob = "  "; if (index+1 < file.length) loader.methods.RecProbels(index+1);
								for (int i = 0; i < value.length; i++) newfile.add(prob + "- " + value[i]);
								for (int i = index+oldfile.length+1; i < file.length; i++) newfile.add(file[i]);
								this.file = new String[newfile.size()];
								for (int i = 0; i < file.length; i++) file[i] = newfile.get(i);
								writeFile();
							}
						} else { //записываем массив, если его нет в конфиге
							ArrayList<String> newfile = new ArrayList<String>();
							for (int i = 0; i < file.length; i++) newfile.add(file[i]);
							newfile.add(name + ":");
							for (int i = 0; i < value.length; i++) newfile.add("- " + value[i]);
							this.file = new String[newfile.size()];
							for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
							writeFile();
						}
					}
				} else log.info("configWriter: запись значения " + name + " не удалась, массив не найден");
			} else log.info("configWriter: ошибка при записи массива " + name + ", конфиг не установлен");
		}
		
		void setArray(String name, String[] value, boolean skobka) { //для внешних обращений
			setArray(name, value, skobka, -1);
		}
		
		void setOptionInSection(String name, String value, String section) { //запись опции в секцию
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					offLog();
					int index = loader.methods.RecIndexInSection(section, name);
					onLog();
					if (index != -1) setOption(name, value, index); else { //если опции нет в конфиге (ее не надо заменять)
						ArrayList<String> newfile = new ArrayList<String>(); //запись секции, если ее нет в конфиге
						for (int i = 0; i < file.length; i++) newfile.add(file[i]);
						int sec = loader.methods.RecIndexSection(section);
						if (sec == -1) { //если создается новая секция
							newfile.add(section + ":");
							newfile.add("  " + name + ": " + value);
						} else if (sec + loader.getSectionRealLength(section) == file.length-1) { //если нужно добавить в старую, и она в конце конфига
							newfile.add("  " + name + ": " + value);
						} else { //если позиция плавающая
							newfile.clear();
							for (int i = 0; i < sec; i++) newfile.add(file[i]);
							for (int i = sec; i <= sec + loader.getSectionRealLength(section) & i < file.length; i++) newfile.add(file[i]);
							newfile.add("  " + name + ": " + value);
							for (int i = sec + loader.getSectionRealLength(section)+1; i < file.length; i++) newfile.add(file[i]);
						}
						this.file = new String[newfile.size()];
						for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
						writeFile();
					}
				} else {
					this.file = new String[2];
					this.file[0] = section + ":";
					this.file[1] = "  " + name + ": " + value;
					writeFile();
					this.neew = false;
				}
			} else log.info("configWriter: ошибка при записи параметра " + name + " в секцию " + section + ", конфиг не установлен");
		}
		
		void setArrayInSection(String name, String value[], boolean skobka, String section) { //запись массива в секцию
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					offLog();
					int index = loader.methods.RecIndexInSection(section, name);
					onLog();
					if (index != -1) setArray(name, value, skobka, index); else {
						ArrayList<String> newfile = new ArrayList<String>(); //запись массива, если его нет в конфиге
						for (int i = 0; i < file.length; i++) newfile.add(file[i]);
						int sec = loader.methods.RecIndexSection(section);
						if (sec == -1) { //если создается новая секция
							newfile.add(section + ":");
							if (!skobka) newfile.add("  " + name + ":"); else newfile.add("  " + name + ": []");
							if (!skobka) newfile.add("  - process write");
						} else if (sec + loader.getSectionRealLength(section) == file.length-1) { //если нужно добавить в старую, и она в конце конфига
							if (!skobka) newfile.add("  " + name + ":"); else newfile.add("  " + name + ": []");
							if (!skobka) newfile.add("  - process write");
						} else { //если позиция плавающая
							newfile.clear();
							for (int i = 0; i < sec; i++) newfile.add(file[i]);
							for (int i = sec; i <= sec + loader.getSectionRealLength(section) & i < file.length; i++) newfile.add(file[i]);
							if (!skobka) newfile.add("  " + name + ":"); else newfile.add("  " + name + ": []");
							if (!skobka) newfile.add("  - process write");
							for (int i = sec + loader.getSectionRealLength(section) + 1; i < file.length; i++) newfile.add(file[i]);
						}
						this.file = new String[newfile.size()];
						for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
						loader.fakeload(file);
						index = loader.methods.RecIndexInSection(section, name);
						setArray(name, value, skobka, index);
					}
				} else {
					if (skobka) {
						this.file = new String[2];
						this.file[0] = section + ":";
						String send = "  " + name + ": [ " + value[0];
						for (int i = 1; i < value.length; i++) { //парсим в строку
							send += ", " + value[i]; 
						}
						send += " ]";
						this.file[1] = send;
						writeFile();
						this.neew = false;
					} else {
						this.file = new String[value.length + 2];
						this.file[0] = section + ":";
						this.file[1] = "  " + name + ":";
						for (int i = 2; i < file.length; i++) {
							this.file[i] = "  - " + value[i-2];
						}
						writeFile();
						this.neew = false;
					}
				}
			} else log.info("configWriter: ошибка при записи массива " + name + " в секцию " + section + ", конфиг не установлен");
		}
		
		private void delOption(String name, int ind) { //удаление опции из конфига
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					int index = ind; if (index == -1) index = loader.methods.RecIndexNoSection(name);
					if (index > -1) {
						ArrayList<String> newfile = new ArrayList<String>();
						for (int i = 0; i < index; i++) newfile.add(file[i]);
						for (int i = index+1; i < file.length; i++) newfile.add(file[i]);
						this.file = new String[newfile.size()];
						for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
						writeFile();
					} else log.info("configWriter: ошибка при удалении переменной " + name + ", переменная не найдена");
				} else log.info("configWriter: ошибка при удалении переменной " + name + ", конфиг отсутствует");
			} else log.info("configWriter: ошибка при удалении переменной " + name + ", конфиг не установлен");
		}
		
		void delOption(String name) { //для внешних обращений
			delOption(name, -1);
		}
		
		private void delArray(String name, int ind) { //удаление массива
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					int index = ind; if (index == -1) index = loader.methods.RecIndexNoSection(name);
					if (index > -1) {
						boolean skobka = loader.methods.IsArray(file[index], index).isSkobka;
						if (skobka) {
							delOption(name, index);
						} else {
							int leng = loader.methods.RecStringArray(index).length;
							ArrayList<String> newfile = new ArrayList<String>();
							for (int i = 0; i < index; i++) newfile.add(file[i]);
							for (int i = index+leng+1; i < file.length; i++) newfile.add(file[i]);
							this.file = new String[newfile.size()];
							for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
							writeFile();
						}
					} else log.info("configWriter: ошибка при удалении массива " + name + ", массив не найден");
				} else log.info("configWriter: ошибка при удалении массива " + name + ", конфиг отсутствует");
			} else log.info("configWriter: ошибка при удалении массива " + name + ", конфиг не установлен");
		}
		
		void delArray(String name) { //для внешних обращений
			delArray(name, -1);
		}
		
		void delOptionInSection(String name, String section) { //удаление параметра из секции
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					int index = loader.methods.RecIndexInSection(section, name);
					if (index > -1) {
						delOption(name, index);
					} else log.info("configWriter: ошибка при удалении переменной " + name + " из секции " + section + ", переменная не найдена");
				} else log.info("configWriter: ошибка при удалении переменной " + name + " из секции " + section + ", конфиг отсутствует");
			} else log.info("configWriter: ошибка при удалении переменной " + name + " из секции " + section + ", конфиг не установлен");
		}
		
		void delArrayInSection(String name, String section) { //удаление массива из секции
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					int index = loader.methods.RecIndexInSection(section, name);
					if (index > -1) {
						delArray(name, index);
					} else log.info("configWriter: ошибка при удалении массива " + name + " из секции " + section + ", массив не найден");
				} else log.info("configWriter: ошибка при удалении массива " + name + " из секции " + section + ", конфиг отсутствует");
			} else log.info("configWriter: ошибка при удалении массива " + name + " из секции " + section + ", конфиг не установлен");
		}
		
		void delSection(String section) {
			if (set && patch != null) {
				if (!neew) {
					configLoader loader = new configLoader();
					loader.fakeload(file);
					int index = loader.methods.RecIndexSection(section);
					if (index > -1) {
						int leng = loader.getSectionRealLength(section);
						ArrayList<String> newfile = new ArrayList<String>();
						for (int i = 0; i < index; i++) newfile.add(file[i]);
						for (int i = index+leng+1; i < file.length; i++) newfile.add(file[i]);
						this.file = new String[newfile.size()];
						for (int i = 0; i < file.length; i++) this.file[i] = newfile.get(i);
						writeFile();
					} else log.info("configWriter: ошибка при удалении секции " + section + ", секция не найдена");
				} else log.info("configWriter: ошибка при удалении секции " + section + ", конфиг отсутствует");
			} else log.info("configWriter: ошибка при удалении секции " + section + ", конфиг не установлен");
		}
		
		boolean removeConfig() { //удалить весь конфиг (сам файл)
			boolean result = false;
			if (set && patch != null) {
				if (!neew) {
					File conf = new File(patch);
					if (conf.isFile()) result = conf.delete(); else log.info("configWriter: ошибка при удалении конфига, конфиг не существует");
				} else log.info("configWriter: ошибка при удалении конфига, конфиг не существует");
			} else log.info("configWriter: ошибка при удалении конфига, конфиг не установлен");
			return result;
		}
		
		class methods { //класс со внутренними методами
			void SetOption(String name, String value, int index) { //установить опцию по индексу
				setOption(name, value, index);
			}
			void SetArray(String name, String[] value, boolean skobka, int index) { //установить массив по индексу
				setArray(name, value, skobka, index);
			}
			void DelOption(String name, int index) { //удалить опцию по индексу
				delOption(name, index);
			}
			void DelArray(String name, int index) { //удалить массив по индексу
				delArray(name, index);
			}
			methods getMethods() {methods m = new methods(); return m;} //получить весь класс
		}
		methods methods = new methods();
		
		configWriter getWriter() { //получить весь класс
			configWriter w = new configWriter();
			return w;
		}
		
		void writeArray(String[] array) { //запись массива строк в файл как есть (создает новый конфиг или полностью перезаписывает текущий)
			if (set && patch != null) {
				if (array != null) {
					if (array.length > 0) {
						this.file = array;
						writeFile();
						setConfig(patch); //обновляем инфу
					}
				}
			} else log.info("configWriter: ошибка при записи массива, конфиг не установлен");
		}
	}
	
	class FileDataBase { //класс-основа для создания БД на файле
		ArrayList<String> base = new ArrayList<String>();
		
		void read(String file) { //чтение файла
			File f = new File(file);
			if (f.isFile()) {
				if (!base.isEmpty()) base.clear();
				try {
					BufferedReader reader = new BufferedReader(new FileReader(f));
					String line = null; int l = 0;
					do {
						line = reader.readLine();
						l++;
						if (line != null) base.add(line);
					} while (line != null && l < 2147483647);
					reader.close();
				} catch (Exception e) {e.printStackTrace();}
			} else log.info("FileDataBase: file " + file + " not found");
		}
		
		void write(String file) { //запись файла
			if (!base.isEmpty()) {
				try {
					BufferedWriter writer = new BufferedWriter(new FileWriter(file));
					for (int i = 0; i < base.size(); i++) {
						if (i > 0) writer.newLine();
						writer.write(base.get(i));
					}
					writer.close();
				} catch (Exception e) {e.printStackTrace();}
			} else log.info("FileDataBase: empty base " + file);
		}
		
		ArrayList<String> get() { //получение ссылки на ArrayList
			return this.base;
		}
		
		FileDataBase getBase() { //получение всего класса
			FileDataBase db = new FileDataBase();
			return db;
		}
	}
}