package InterVi.AllFixer;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.entity.HumanEntity;

import java.util.Arrays;

public class antishare implements Listener {
	AllFixer main;
	private utils ut = new utils("mem"); utils ut2 = new utils("mem");
	antishare(AllFixer all) {
		main = all;
		ut.mem.setSize(200);
		ut2.mem.setSize(200);
		main.log.info("мониторинг раздач активирован");
	}
	
	//поднятие
		@EventHandler(priority = EventPriority.LOWEST)
		public void pickupItem(PlayerPickupItemEvent event) {
			if (main.conf.antishare == true && event.isCancelled() == false) {
			int item = event.getItem().getItemStack().getData().getItemType().getId();
			String sitem = event.getItem().getItemStack().getData().getItemType().name();
			int amount = event.getItem().getItemStack().getAmount();
			Player player = event.getPlayer();
			String mir = event.getItem().getLocation().getWorld().getName();
			int x = event.getItem().getLocation().getBlockX();
			int y = event.getItem().getLocation().getBlockY();
			int z = event.getItem().getLocation().getBlockZ();
			String loc = mir + "; " + x + "; " + y + "; " + z;
				for (int i = 0; i < main.conf.share.length; i++) {
					if (item == main.conf.share[i] && amount >= main.conf.amount && player != null && player.isOp() == false) {
						if (ut.chW(mir, main.conf.shareworlds) == true) break;
						if (main.conf.console == true && main.conf.antitocons == true) main.log.info("игрок " + player.getName() + " поднял " + sitem + " (" + amount + ") на " + loc);
						if (main.conf.mesingame == true && main.conf.antitogame == true) main.sendMes("игрок " + player.getName() + " поднял " + sitem + " (" + amount + ") на " + loc);
						if (main.conf.gamelog == true) main.sendGlog(player.getName(), "поднял " + sitem + " (" + amount + ") на " + loc);
						break;
					}
				}
			}
		}
		
		//выбрасывание
		@EventHandler(priority = EventPriority.LOWEST)
		public void shareItem(PlayerDropItemEvent event) {
			if (main.conf.antishare == true && event.isCancelled() == false) {
			int item = event.getItemDrop().getItemStack().getData().getItemType().getId();
			String sitem = event.getItemDrop().getItemStack().getData().getItemType().name();
			int amount = event.getItemDrop().getItemStack().getAmount();
			Player player = event.getPlayer();
			String mir = event.getItemDrop().getLocation().getWorld().getName();
			int x = event.getItemDrop().getLocation().getBlockX();
			int y = event.getItemDrop().getLocation().getBlockY();
			int z = event.getItemDrop().getLocation().getBlockZ();
			String loc = mir + "; " + x + "; " + y + "; " + z;
				for (int i = 0; i < main.conf.share.length; i++) {
					if (item == main.conf.share[i] && amount >= main.conf.amount && player != null) {
						if (ut.chW(mir, main.conf.shareworlds) == true) break;
						if (main.conf.console == true && main.conf.antitocons == true) main.log.info("игрок " + player.getName() + " раздал " + sitem + " (" + amount + ") на " + loc);
						if (main.conf.mesingame == true && main.conf.antitogame == true) main.sendMes("игрок " + player.getName() + " раздал " + sitem + " (" + amount + ") на " + loc);
						if (main.conf.gamelog == true) main.sendGlog(player.getName(), "раздал " + sitem + " (" + amount + ") на " + loc);
						break;
					}
				}
			}
		}
		
		//при кликах в инвентаре
		@EventHandler(priority = EventPriority.LOWEST)
		public void open(InventoryClickEvent event) {
			if (main.conf.antishare == true) {
			String inb, enb, opb;
			HumanEntity player = event.getWhoClicked();
			String mir = player.getLocation().getWorld().getName();
			int x = player.getLocation().getBlockX();
			int y = player.getLocation().getBlockY();
			int z = player.getLocation().getBlockZ();
			String loc = mir + "; " + x + "; " + y + "; " + z;
			int check = loc.hashCode();
			if (event.isRightClick() == true | event.isLeftClick() == true | event.isShiftClick() == true && player != null && player.isOp() == false && ut.mem.isVal(check) == false && ut.chW(mir, main.conf.shareworlds) == false) {
				inb = null; enb = null; opb = null;
				ut.mem.sendValue(check); //чтобы не спамило при множестве кликов + снижение нагрузки
				//получение инвентарей в ItemStack
				ItemStack inv[] = new ItemStack[player.getInventory().getContents().length]; inv = player.getInventory().getContents(); int invb[] = new int[inv.length];
				ItemStack ender[] = new ItemStack[player.getEnderChest().getContents().length]; ender = player.getEnderChest().getContents(); int end[] = new int[ender.length];
				int slot = event.getInventory().getContents().length;
				ItemStack opitems[] = new ItemStack[slot]; opitems = event.getInventory().getContents(); int openslot[] = new int[slot];
				//исключения
				if (Arrays.equals(inv, opitems) == true) {for (int i = 0; i < slot; i++) opitems[i] = null;}; //если игрок копошится в одиночном инвентаре
				if (Arrays.equals(ender, opitems) == true) {for (int i = 0; i < slot; i++) opitems[i] = null;}; //если игрок открыл эндер сундук
				//конвертация в int (массивы id блоков)
				for (int i = 0; i < inv.length; i++) {if (inv[i] != null) invb[i] = inv[i].getTypeId();}; //инвентарь игрока
				for (int i = 0; i < ender.length; i++) {if (ender[i] != null) end[i] = ender[i].getTypeId();}; //эндер сундук игрока
				for (int i = 0; i < slot; i++) {if (opitems[i] != null) openslot[i] = opitems[i].getTypeId();}; //открытый инвентарь
				//обнаружение блоков
				for (int i = 0; i < inv.length; i++) { //в инвентаре
					for (int i2 = 0; i2 < main.conf.share.length; i2++) {
						if (inv[i] == null) continue;
						if (invb[i] == main.conf.share[i2] && inv[i].getAmount() >= main.conf.hamount) {
							if (inb == null) inb = ": ";
							inb = inb + inv[i].getType().toString() + " (" + inv[i].getAmount() + "), ";
						}
				}}
				
				for (int i = 0; i < end.length; i++) { //в эндер сундуке
					for (int i2 = 0; i2 < main.conf.share.length; i2++) {
						if (ender[i] == null) continue;
						if (end[i] == main.conf.share[i2] && ender[i].getAmount() >= main.conf.hamount) {
							if (enb == null) enb = ": ";
							enb = enb + ender[i].getType().toString() + " (" + ender[i].getAmount() + "), ";
						}
				}}
				
				for (int i = 0; i < openslot.length; i++) {  //в открытом инвентаре
					for (int i2 = 0; i2 < main.conf.share.length; i2++) {
						if (opitems[i] == null) continue;
						if (openslot[i] == main.conf.share[i2] && opitems[i].getAmount() >= main.conf.hamount) {
							if (opb == null) opb = ": ";
							opb = opb + opitems[i].getType().toString() + " (" + opitems[i].getAmount() + "), ";
						}
				}}
				//для антифлудера
				String inb2 = inb + player.getName();
				String enb2 = enb + player.getName();
				String opb2 = opb + player.getName();
				//обрезка запятых
				if (inb != null) inb = ut.remChars(inb, inb.length() - 2, inb.length());
				if (enb != null) enb = ut.remChars(enb, enb.length() - 2, enb.length());
				if (opb != null) opb = ut.remChars(opb, opb.length() - 2, opb.length());
				//вывод в консоль, если указанные блоки в указанном количестве найдены
				if (main.conf.console == true && inb != null && main.conf.antitocons == true && ut2.mem.isVal(inb2.hashCode()) == false) {main.log.info("у игрока " + ((Player) player).getName() + " в инвентаре обнаружен(ы)" + inb + " на " + loc);};
				if (main.conf.console == true && enb != null && main.conf.antitocons == true && ut2.mem.isVal(enb2.hashCode()) == false) {main.log.info("у игрока " + ((Player) player).getName() + " в эндер сундуке обнаружен(ы)" + enb + " на " + loc);};
				if (main.conf.console == true && opb != null && main.conf.antitocons == true && ut2.mem.isVal(opb2.hashCode()) == false) {main.log.info("у игрока " + ((Player) player).getName() + " в хранилище обнаружен(ы)" + opb + " на " + loc);};
				//отправка сообщений в игру
				if (main.conf.mesingame == true && inb != null && main.conf.antitogame == true && ut2.mem.isVal(inb2.hashCode()) == false) {main.sendMes("у игрока " + ((Player) player).getName() + " в инвентаре обнаружен(ы)" + inb + " на " + loc);};
				if (main.conf.mesingame == true && enb != null && main.conf.antitogame == true && ut2.mem.isVal(enb2.hashCode()) == false) {main.sendMes("у игрока " + ((Player) player).getName() + " в эндер сундуке обнаружен(ы)" + enb + " на " + loc);};
				if (main.conf.mesingame == true && opb != null && main.conf.antitogame == true && ut2.mem.isVal(opb2.hashCode()) == false) {main.sendMes("у игрока " + ((Player) player).getName() + " в хранилище обнаружен(ы)" + opb + " на " + loc);};
				//отправка в виртуальный лог
				if (main.conf.mesingame == true && inb != null && main.conf.antitogame == true && ut2.mem.isVal(inb2.hashCode()) == false) main.sendGlog(player.getName(), "в инвентаре обнаружен(ы)" + inb + " на " + loc);
				if (main.conf.mesingame == true && enb != null && main.conf.antitogame == true && ut2.mem.isVal(enb2.hashCode()) == false) main.sendGlog(player.getName(), "в эндер сундуке обнаружен(ы)" + enb + " на " + loc);
				if (main.conf.mesingame == true && opb != null && main.conf.antitogame == true && ut2.mem.isVal(opb2.hashCode()) == false) main.sendGlog(player.getName(), "в хранилище обнаружен(ы)" + opb + " на " + loc);
				//для предотвращения флуда об одинаковых итемах (щедящий режим)
				if (main.conf.antishre == true) {
					if (inb != null) {ut2.mem.sendValue(inb2.hashCode()); if (ut2.mem.colVal(inb.hashCode()) >= 20) ut2.mem.delVal(inb.hashCode());};
					if (enb != null) {ut2.mem.sendValue(enb2.hashCode()); if (ut2.mem.colVal(enb.hashCode()) >= 20) ut2.mem.delVal(enb.hashCode());};
					if (opb != null) {ut2.mem.sendValue(opb2.hashCode()); if (ut2.mem.colVal(opb.hashCode()) >= 20) ut2.mem.delVal(opb.hashCode());};
			} else ut2.mem.reset();
			}
		}}
}